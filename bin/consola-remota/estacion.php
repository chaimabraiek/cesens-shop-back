#!/usr/bin/env php
<?php

namespace Encore\CesensShop\Bin\ConsolaRemota;

use Encore\Application\Application;
use Encore\CesensShop\Model\Db\NodosDb;
use Encore\CesensShop\Model\Db\UbicacionesDb;

global $argv, $argc;
if ($argc < 3) {
    error('Número de parámetros incorrecto.');
    exit(254);
}
if ($argv[1] !== 'enable' && $argv[1] !== 'disable') {
    error('Parámetro 1 incorrecto: Debe ser "enable" o "disable".');
    exit(254);
}
if (!is_numeric($argv[2])) {
    error('El parámetro 2 no es correcto: Debe ser un número');
    exit(254);
}

chdir(dirname(dirname(__DIR__)));
require_once dirname(dirname(__DIR__)) . '/vendor/encore/encore-framework/lib/Encore/Application/Application.php';
$app = new Application();

$db  = $app->getConfig('database');
$pdo = new \PDO($db['dsn'],  $db['user'], $db['passwd'], [
    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING,
    \PDO::ATTR_EMULATE_PREPARES => false,
]);
$ndb = new NodosDb($pdo);
$udb = new UbicacionesDb($pdo);

$nodo = $ndb->get($argv[2]);
if ($nodo === null) {
    error('Nodo no encontrado.');
    exit(1);
}

$nodo->setConsolaRemota($argv[1] === 'enable');
if ($ndb->save($nodo)) {
    $ubicacion = $udb->getByNodo($nodo);
    if ($ubicacion === null) {
        echo 'Este nodo no está asociado a ninguna ubicación', PHP_EOL;
    } else {
        echo 'Ubicación: ', $ubicacion->getNombre(), PHP_EOL;
        echo 'Cliente: ', $ubicacion->getCliente()->getNombre(), PHP_EOL;
        echo 'Último envío: ', $ubicacion->getUltimoDato('Y-m-d H:i:s'), PHP_EOL;
    }
    exit(0);
} else {
    error('Error al guardar estado del nodo');
    exit(1);
}

function error($msg) {
    $stderr = fopen('php://stderr', 'w+');
    if ($stderr) {
        fwrite($stderr, $msg . PHP_EOL);
        fclose($stderr);
    }
}