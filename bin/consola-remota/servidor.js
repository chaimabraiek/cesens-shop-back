#!/usr/bin/env node

"use strict";

const net = require('net');
const exec = require('child_process').exec;

var estaciones = {
    lista: {},
    añadirEstacion: function (estacion, socket, fn) {
        if (this.lista[estacion] !== undefined) {
            if (this.lista[estacion].estacion !== null) {
                console.error('Esta estación ya estaba añadida en la lista', estacion);
                return false;
            }
            this.lista[estacion].estacion = socket;
            if (fn) {
                fn(estacion);
            }
        } else {
            this.lista[estacion] = {
                estacion: socket,
                clientes: []
            };
        }
        return true;
    },
    esperarEstacion: function (estacion, cliente, fn) {
        if (this.existe(estacion)) {
            fn(true, estacion, cliente);
        } else {
            var cmd = './estacion.php enable ' + estacion;
            exec(cmd, function (error, stdout, stderr) {
                if (error) {
                    var f = fn ? 'write' : 'end';
                    cliente[f](stderr);
                    fn(false, estacion, cliente);
                } else {
                    if (stdout) {
                        cliente.write(stdout);
                    }
                    cliente.write('Esperando a que la estación se conecte.\n');
                    this.lista[estacion] = {
                        estacion: null,
                        clientes: [cliente]
                    };
                    fn(true, estacion, cliente);
                }
            }.bind(this));
        }
    },
    añadirCliente: function (estacion, cliente) {
        if (this.existe(estacion)) {
            this.lista[estacion].clientes.push(cliente);
            return true;
        } else {
            console.error('La estación no existe.');
            return false;
        }
    },
    eliminarEstacion: function (estacion, fnClientes) {
        if (fnClientes) {
            var cmd = './estacion.php disable ' + estacion;
            exec(cmd, function (error, stdout, stderr) {
                if (error) {
                    console.log(error, stdout, stderr);
                }
            });
            this.forEachCliente(estacion, fnClientes);
        }
        delete this.lista[estacion];
    },
    eliminarCliente: function (estacion, cliente) {
        if (this.existe(estacion)) {
            var clientes = this.lista[estacion].clientes;
            this.lista[estacion].clientes.splice(clientes.indexOf(cliente), 1);
        }
    },
    existe: function (estacion) {
        return this.lista[estacion] !== undefined;
    },
    estaEsperando: function (estacion) {
        return this.existe(estacion) && this.lista[estacion].estacion === null;
    },
    getEstacion: function (estacion, fn) {
        if (this.existe(estacion)) {
            var e = this.lista[estacion].estacion;
            if (e && fn) {
                fn(e);
            }
            return e;
        } else {
            return null;
        }
    },
    getClientes: function (estacion) {
        return this.lista[estacion] !== undefined ? this.lista[estacion].clientes : null;
    },
    forEachCliente: function (estacion, fn) {
        var clientes = this.getClientes(estacion);
        if (clientes) {
            for (var i = 0, n = clientes.length; i < n; i++) {
                fn(clientes[i]);
            }
        }
    },
    forEachEstacion: function (fn) {
        for (var id in this.lista) {
            if (this.lista.hasOwnProperty(id)) {
                fn(this.lista[id].estacion, id);
            }
        }
    },
    listar: function () {
        return Object.keys(this.lista);
    }
};

// Servidor para las estaciones
var ssEstaciones = net.createServer(function (socket) {
    socket.setEncoding('utf8');

    var id = undefined;
    socket.on('data', function (data) {
        if (id === undefined) {
            id = data.substring(0, data.indexOf('\n')).trim();
            estaciones.añadirEstacion(id, socket, function (estacion) {
                estaciones.forEachCliente(estacion, function (cliente) {
                    cliente.write('Conectado a la estación ' + estacion + '.\n');
                });
            });
        } else {
            estaciones.forEachCliente(id, function (cliente) {
                cliente.write(data);
            });
        }
    });
    socket.on('end', function () {
        estaciones.eliminarEstacion(id, function (cliente) {
            cliente.end('Conexión cerrada por la estación.\n');
        });
    });
});

// ServerSocket para los clientes
var ssClientes = net.createServer(function (socket) {
    socket.setEncoding('utf8');

    var estacion = undefined;
    var disponibles = estaciones.listar();
    if (disponibles.length === 0) {
        socket.write('No hay ninguna estación disponible en este momento.\n');
    } else {
        socket.write('Listado de estaciones disponibles:\n');
        estaciones.forEachEstacion(function (estacion, id) {
            var msg = estaciones.getClientes(id).length + ' clientes conectados';
            if (estacion === null) {
                msg += ' (esperando a la estación)';
            }
            msg += '.';
            socket.write(id + '\t' + msg + '\n');
        });
    }
    socket.write('\n');
    socket.write('Puede conectarse a una estación que no esté disponible en este momento. En ese caso, deberá esperar a que ésta se ponga en modo de consola remota. Cuando la estación se conecte, inmediatamente podrá enviarle comandos.\n');
    socket.write('Introduzca el identificador de la estación al que conectar: ');
    socket.on('data', function (data) {
        if (estacion === undefined) {
            data = data.trim();
            if (estaciones.existe(data)) {
                estacion = data;
                estaciones.añadirCliente(estacion, socket);
                socket.write('Conectado a la estación ' + data + '.\n');
            } else {
                estacion = null;
                estaciones.esperarEstacion(data, socket, function (resultado) {
                    if (resultado) {
                        socket.write('Esperando a la estación...\n');
                        estacion = data;
                    } else {
                        socket.end('Identificador de estación no válido.\n');
                    }
                });
            }
        } else if (estacion !== null) {
            estaciones.getEstacion(estacion, function (estacion) {
                estacion.write(data);
            });
        }
    });
    socket.on('end', function () {
        if (estacion) {
            estaciones.eliminarCliente(estacion, socket);
        }
    });
});

ssEstaciones.listen(50000);
ssClientes.listen(50001);

// Capturar señal de interrupción SIGINT
process.on('SIGINT', function () {
    console.log('Apangando servidor...');
    ssEstaciones.close();
    ssClientes.close();
    console.log('Server sockets cerrados.');
    estaciones.forEachEstacion(function (estacion, id) {
        if (estacion !== null) {
            estacion.end('salir servidor\n');
        }
        estaciones.eliminarEstacion(id, function (cliente) {
            cliente.end('Conexión cerrada por el servidor.\n');
        });
    });
    console.log('Todas las conexiones han sido cerradas.');
    process.exit();
});
