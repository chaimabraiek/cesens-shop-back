<?php

namespace Encore\CesensShop\Bin;

date_default_timezone_set('Europe/Madrid');

define('CONTACT_EMAIL', 'carlos@encore-lab.com');
define('LOG_PATH', dirname(dirname(__DIR__)) . '/data/log');
define('LOG_FILE', LOG_PATH . '/update.log');
define('SNAPSHOT_PATH', dirname(dirname(__DIR__)) . '/data/database/snapshots');
define('SNAPSHOT_FILE', SNAPSHOT_PATH . '/' . getVersion() . '_' . date('Y-m-d_H-i-s') . '.sql');
define('DATA_FILE', tempnam(sys_get_temp_dir(), 'idpro_update_data'));

function getConfig()
{
    static $config = null;
    if ($config === null) {
        $config = include dirname(dirname(__DIR__)) . '/config/config.php';
    }
    return $config;
}

function getVersion()
{
    $config = getConfig();
    return !empty($config['version']['number'])
        ? $config['version']['number'] : null;
}

function rollback($settings, $snapshot = null)
{
    return shell_exec(prepareCommand(
        'mysql --host="%host" --user="%user" --password="%passwd" %dbname < '
            . ($snapshot === null ? SNAPSHOT_FILE : $snapshot),
        $settings
    ));
}

function prepareCommand($command, $settings)
{
    foreach ($settings as $name => $value) {
        $command = str_replace('%' . $name, $value, $command);
    }
    return $command;
}

function getMysqlSettings()
{
    static $config = null;
    if ($config === null) {
        $config = include dirname(dirname(__DIR__)) . '/config/bd.php';
    }
    $settings = [];
    $dsn      = explode(';', substr($config['dsn'], strpos($config['dsn'], ':') + 1));
    foreach ($dsn as $pair) {
        list($name, $value) = explode('=', $pair);
        $settings[$name] = $value;
    }
    $settings['user']   = $config['user'];
    $settings['passwd'] = $config['passwd'];
    return $settings;
}

function ensurePath($path)
{
    if (!is_dir($path)) {
        if (!mkdir($path, 0755, true)) {
            return false;
        }
    }
    return true;
}

function sendEmail($message)
{
    return mail(CONTACT_EMAIL, 'Update error at ' . $_ENV['HOSTNAME'] . dirname(dirname(__DIR__)), $message);
}

class Database
{
    private $settings;
    private $snapshot;
    private $baseDir;

    public static function createFromFile($file, $snapshotFile)
    {
        $config   = include $file;
        $settings = [];
        $dsn      = explode(';', substr($config['dsn'], strpos($config['dsn'], ':') + 1));
        foreach ($dsn as $pair) {
            list($name, $value) = explode('=', $pair);
            $settings[$name] = $value;
        }
        $settings['user']   = $config['user'];
        $settings['passwd'] = $config['passwd'];
        return new Database($settings, $snapshotFile);
    }

    public function __construct(array $settings, $snapshot)
    {
        $this->settings = $settings;
        $this->snapshot = $snapshot;
        $this->dataFile = tempnam(sys_get_temp_dir(), 'idpro_update_data');
        $this->baseDir  = dirname(dirname(__DIR__));

        if (!ensurePath(dirname($this->snapshot))) {
            throw new \Exception('No se ha podido asegurar la ruta para el snapshot', 2);
        }
        if (!touch($this->snapshot)) {
            throw new \Exception('El snapshot no se puede escribir', 3);
        }
    }

    public function snapshot()
    {
        return shell_exec($this->prepareCommand(
            'mysqldump --add-drop-database --add-drop-table --allow-keywords --complete-insert --create-options --extended-insert --single-transaction --skip-comments --host="%host" --user="%user" --password="%passwd" --databases %dbname > ' . $this->snapshot,
            $this->settings
        ));
    }

    public function exportarDatos()
    {
        return shell_exec($this->prepareCommand(
            'mysqldump --allow-keywords --complete-insert --insert-ignore --no-create-info --extended-insert --no-create-db --single-transaction --skip-comments --host="%host" --user="%user" --password="%passwd" --databases %dbname > ' . $this->dataFile,
            $this->settings
        ));
    }

    public function volcarEstructura()
    {
        return shell_exec($this->prepareCommand(
            'mysql --host="%host" --user="%user" --password="%passwd" %dbname < ' . $this->baseDir . '/data/database/estructura.sql',
            $this->settings
        ));
    }

    public function volcarDatos()
    {
        return shell_exec($this->prepareCommand(
            'mysql --host="%host" --user="%user" --password="%passwd" %dbname < ' . $this->baseDir . '/data/database/reset.sql',
            $this->settings
        ));
    }

    public function volcarDatosExportados()
    {
        return shell_exec($this->prepareCommand(
            'mysql --host="%host" --user="%user" --password="%passwd" %dbname < ' . $this->dataFile,
            $this->settings
        ));
    }

    public function rollback()
    {
        return shell_exec($this->prepareCommand(
            'mysql --host="%host" --user="%user" --password="%passwd" %dbname < ' . $this->snapshot,
            $this->settings
        ));
    }

    private function prepareCommand($command, $settings)
    {
        foreach ($settings as $name => $value) {
            $command = str_replace('%' . $name, $value, $command);
        }
        return $command;
    }
}

class Log
{
    const LEVEL_INFO    = 'INFO   ';
    const LEVEL_WARNING = 'WARNING';
    const LEVEL_ERROR   = 'ERROR  ';

    private $file;
    private $format;

    public function __construct($file, $format = '%l [%t] %m')
    {
        $this->file = $file;
        if (!is_file($file)) {
            if (!touch($file)) {
                throw new \Exception('Couldn\'t create the log file');
            }
        }
    }

    public function info($message)
    {
        return $this->log(self::LEVEL_INFO, $message);
    }

    public function warning($message)
    {
        return $this->log(self::LEVEL_WARNING, $message);
    }

    public function error($message)
    {
        return $this->log(self::LEVEL_ERROR, $message);
    }

    protected function log($level, $message)
    {
        $line = str_replace('%l', $level, $this->format);
        $line = str_replace('%t', date('Y-m-d H:i:s'), $line);
        $line = str_replace('%m', $message, $line);
        if (!file_put_contents($this->file, $line . PHP_EOL, FILE_APPEND)) {
            throw new \Exception('Couldn\'t write to log file');
        }
        return $line;
    }
}
