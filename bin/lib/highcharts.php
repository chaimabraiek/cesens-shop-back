<?php

function grafica($config, $devolverBase64 = true, $ancho = 1000, $alto = 400, $agrupar = false)
{
    $plantilla = file_get_contents(dirname(dirname(__DIR__)) . '/data/highcharts/grafica.html');
    $plantilla = str_replace('{{CONFIG}}', $config, $plantilla);
    if ($agrupar) {
        $plantilla = str_replace('dataGrouping: {enabled: false},', '', $plantilla);
    }
    $html = dirname(dirname(__DIR__)) . '/data/highcharts/tmp/grafica_' . md5(time() + microtime(true)) . '.html';
    $imagen = $html . '.png';
    file_put_contents($html, $plantilla);
    $cmd = sprintf(
        'phantomjs %s %s %s %dpx*%dpx',
        escapeshellarg(dirname(dirname(__DIR__)) . '/data/phantomjs/rasterize.js'),
        escapeshellarg('file://' . realpath($html)),
        escapeshellarg($imagen),
        $ancho,
        $alto
    );
    exec($cmd, $salida);
    unlink($html);
    if (file_exists($imagen)) {
        if ($devolverBase64) {
            $base64 = base64_encode(file_get_contents($imagen));
            unlink($imagen);
            return $base64;
        } else {
            return $imagen;
        }
    } else {
        return null;
    }
}
