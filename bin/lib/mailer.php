<?php

require dirname(dirname(__DIR__)) . '/vendor/phpmailer/PHPMailer.php';
require dirname(dirname(__DIR__)) . '/vendor/phpmailer/SMTP.php';

function getMailer()
{
    $mailer = new \PHPMailer();
    $mailer->isSMTP();
    $mailer->CharSet    = 'UTF-8';
    $mailer->Host       = 'mail.cesens.com';
    $mailer->SMTPAuth   = true;
    $mailer->SMTPSecure = '';
    $mailer->SMTPAutoTLS = false;// PHP 5.6 requiere que el certificado sea válido
    $mailer->Username   = 'no-responder@cesens.com';
    $mailer->Password   = 'g3!vZ78i';
    $mailer->setFrom('no-responder@cesens.com', 'Cesens');
    $mailer->addReplyTo('no-responder@cesens.com', 'Cesens');
    $mailer->isHTML(true);
    return $mailer;
}

function getEmailsBcc()
{
    static $lista = 0;
    if ($lista === 0) {
        $lista = [];
        $fichero = dirname(dirname(__DIR__)) . '/config/private/emails-bcc.list';
        if (is_readable($fichero)) {
            foreach (preg_split('/\n|\r\n?/', file_get_contents($fichero)) as $email) {
                $email = trim($email);
                if (!empty($email) && $email[0] !== '#') {
                    $lista[] = $email;
                }
            }
        }
    }
    return $lista;
}