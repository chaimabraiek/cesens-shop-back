<?php

abstract class Resumen
{
    /** @var DateTime */
    protected static $AYER;

    protected $pdo;
    protected $informe;
    protected $grafica;

    public function __construct(\PDO $pdo, \Encore\CesensShop\Model\Informe $informe, \Encore\CesensShop\Model\Grafica $grafica)
    {
        if (self::$AYER === null) {
            self::$AYER = new DateTime('today');
            self::$AYER->modify('-1 day');
        }
        $this->pdo = $pdo;
        $this->informe = $informe;
        $this->grafica = $grafica;
    }

    public abstract function getResumen();

    protected static function htmlTabla($grupos)
    {
        return <<<HTML
<table class="resumen">
$grupos
</table>
HTML;
    }

    protected static function htmlGrupo($nombre, $metricas)
    {
        return <<<HTML
<table>
    <tr>
        <th colspan="2">$nombre</th>
    </tr>
$metricas
</table>
HTML;
    }

    protected static function htmlMetrica($nombre, $valor, $unidad, $decimales = 2)
    {
        if ($valor === null) {
            return null;
        }
        $valor = number_format($valor, $decimales, ',', '.');
        return <<<HTML
    <tr>
        <td>$nombre</td>
        <td>$valor $unidad</td>
    </tr>
HTML;
    }

    protected static function formatFecha(DateTime $fecha)
    {
        $meses = [1 => 'ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];
        return $fecha->format('j') . ' ' . $meses[$fecha->format('n')] . '.';
    }
}

class ResumenUbicacion extends Resumen
{
    private $ubicacion;

    public function __construct(\PDO $pdo, \Encore\CesensBlog\Model\Informe $informe, \Encore\CesensBlog\Model\Grafica $grafica, \Encore\CesensBlog\Model\Ubicacion $ubicacion)
    {
        parent::__construct($pdo, $informe, $grafica);
        $this->ubicacion = $ubicacion;
    }

    public function getResumen()
    {
        $grupos = '';
        $i = 0;
        foreach ($this->getGrupos() as $g) {
            $grupo = null;
            switch ($g) {
                case 1:// temperatura
                    $grupo = $this->getGrupoTemperatura();
                    break;
                case 2:// humedad
                    $grupo = $this->getGrupoHumedad();
                    break;
                case 3:// suelo
                    break;
                case 4:// meteorología
                    $grupo = $this->getGrupoPrecipitaciones();
                    break;
                case 5:// índices climáticos
                    break;
                case 6:// planta
                    break;
                case 7:// parámetros internos
                    break;
                case 8:// modelos predictivos
                    break;
                default:
                    trigger_error('Grupo no válido: ' . $g, E_USER_NOTICE);
            }
            if (!empty($grupo)) {
                if ($i++ % 2 === 0) {
                    $grupos .= '<tr><td>' . $grupo . '</td>';
                } else {
                    $grupos .= '<td>' . $grupo . '</td></tr>';
                }
            }
        }
        if ($i % 2 === 1) {
            $grupos .= '<td></td></tr>';
        }
        return self::htmlTabla($grupos);
    }

    private function getGrupos()
    {
        $grupos = [];
        $metricasUbicacion = $this->ubicacion->filtrarMetricas($this->informe->getMetricas());
        foreach ($metricasUbicacion as $m) {
            $grupos[$m->getGrupo()] = true;
        }
        return array_keys($grupos);
    }

    private function getGrupoTemperatura()
    {
        $sql = <<<SQL
SELECT MIN(valor) AS minima, MAX(valor) AS maxima, AVG(valor) AS media
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
SQL;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
            $this->ubicacion->getId(),
            \Encore\CesensBlog\Model\Metrica::$LISTA['T'],
            self::$AYER->format('Y-m-d 00:00:00'),
            self::$AYER->format('Y-m-d 23:59:59'),
        ]);
        $t = $stmt->fetch(\PDO::FETCH_ASSOC);
        $metricas = '';
        $hayDatos = false;
        if ($t['media'] !== null) {
            $metricas .= self::htmlMetrica('Media ayer:', $t['media'], '°C');
            $hayDatos = true;
        }
        if ($t['minima'] !== null) {
            $metricas .= self::htmlMetrica('Mínima ayer:', $t['minima'], '°C');
            $hayDatos = true;
        }
        if ($t['maxima'] !== null) {
            $metricas .= self::htmlMetrica('Máxima ayer:', $t['maxima'], '°C');
            $hayDatos = true;
        }
        return $hayDatos ? self::htmlGrupo('Temperatura', $metricas) : null;
    }

    private function getGrupoHumedad()
    {
        $muestreo = 60 / ($this->ubicacion->muestreo() ?: 60);
        $metricas = '';
        $metrica = \Encore\CesensBlog\Model\Metrica::create([
            'id' => \Encore\CesensBlog\Model\Metrica::$LISTA['h'],
        ]);
        if ($this->ubicacion->metricaDisponible($metrica)) {
            $sql = <<<SQL
SELECT COUNT(*) - 1 AS horasHumectacion
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
  AND valor >= 10
HAVING COUNT(*) > 1
SQL;
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                $this->ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['h'],
                self::$AYER->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $h = $stmt->fetch(\PDO::FETCH_ASSOC);
            $h = $h['horasHumectacion'] / $muestreo;
            $metricas .= self::htmlMetrica('Humectación >10% ayer:', $h, 'h', 0);
            $dias = $this->informe->getDias() ?: 7;
            $desde = new \DateTime('now');
            $desde->modify('-' . $dias . ' days');
            $stmt->execute([
                $this->ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['h'],
                $desde->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $h = $stmt->fetch(\PDO::FETCH_ASSOC);
            $h = $h['horasHumectacion'] / $muestreo;
            $metricas .= self::htmlMetrica('Humectación >10% en los últimos ' . $dias . ' días:', $h, 'h', 0);
        }
        $metrica = \Encore\CesensBlog\Model\Metrica::create([
            'id' => \Encore\CesensBlog\Model\Metrica::$LISTA['H'],
        ]);
        if ($this->ubicacion->metricaDisponible($metrica)) {
            $sql = <<<SQL
SELECT AVG(valor) AS media
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
SQL;
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                $this->ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['H'],
                self::$AYER->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $h = $stmt->fetch(\PDO::FETCH_ASSOC);
            $metricas .= self::htmlMetrica('Humedad media ayer:', $h['media'], '%');
        }
        return empty($metricas) ? null : self::htmlGrupo('Humedad', $metricas);
    }

    private function getGrupoPrecipitaciones()
    {
        $metricas = '';
        $sql = <<<SQL
SELECT SUM(valor) AS precip
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
SQL;
        $stmt = $this->pdo->prepare($sql);

        if ($this->informe->getDias() > 7 || $this->informe->getDias() < 4) {
            $stmt->execute([
                $this->ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
                self::$AYER->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $p = $stmt->fetch(\PDO::FETCH_ASSOC);
            $metricas .= self::htmlMetrica('Ayer:', $p['precip'], 'l/m²');
            $dias = $this->informe->getDias() ?: 7;
            $desde = new \DateTime('now');
            $desde->modify('-' . $dias . ' days');
            $stmt->execute([
                $this->ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
                $desde->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $p = $stmt->fetch(\PDO::FETCH_ASSOC);
            $metricas .= self::htmlMetrica('Últimos ' . $dias . ' días:', $p['precip'], 'l/m²');
            $desde = new \DateTime('now');
            $desde->modify('-30 days');
            $stmt->execute([
                $this->ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
                $desde->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $p = $stmt->fetch(\PDO::FETCH_ASSOC);
            $metricas .= self::htmlMetrica('Últimos 30 días:', $p['precip'], 'l/m²');
        } else {
            $desde = new \DateTime('today');
            $desde->sub(new DateInterval('P' . $this->informe->getDias() . 'D'));
            while ($desde <= self::$AYER) {
                $stmt->execute([
                    $this->ubicacion->getId(),
                    \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
                    $desde->format('Y-m-d') . ' 00:00:00',
                    $desde->format('Y-m-d') . ' 23:59:59',
                ]);
                $p = $stmt->fetch(\PDO::FETCH_ASSOC);
                $metricas .= self::htmlMetrica(self::formatFecha($desde), $p['precip'], 'l/m²');
                $desde->add(new DateInterval('P1D'));
            }
        }
        return self::htmlGrupo('Precipitaciones', $metricas);
    }

}

class ResumenMetrica extends Resumen
{
    private $metrica;

    public function __construct(\PDO $pdo, \Encore\CesensBlog\Model\Informe $informe, \Encore\CesensBlog\Model\Grafica $grafica, \Encore\CesensBlog\Model\Metrica $metrica)
    {
        parent::__construct($pdo, $informe, $grafica);
        $this->metrica = $metrica;
    }

    public function getResumen()
    {
        $grupos = '';
        $i = 0;
        foreach ($this->informe->getUbicaciones() as $ubicacion) {
            $grupo = $this->getValores($ubicacion);
            if (!empty($grupo)) {
                if ($i++ % 2 === 0) {
                    $grupos .= '<tr><td>' . $grupo . '</td>';
                } else {
                    $grupos .= '<td>' . $grupo . '</td></tr>';
                }
            }
        }
        if ($i % 2 === 1) {
            $grupos .= '<td></td></tr>';
        }
        return self::htmlTabla($grupos);
    }

    public function getResumenUbicacion($ubicacion)
    {
        $grupo = $this->getValores($ubicacion);
        if (!empty($grupo)) {
            return '<tr><td>' . $grupo . '</td></tr>';
        } else {
            return '';
        }
    }

    private function getValores(\Encore\CesensBlog\Model\Ubicacion $ubicacion)
    {
        if ($this->metrica instanceof \Encore\CesensBlog\Model\MetricaDerivada) {
            if ($this->metrica->getAcumulativa()) {
                $serie = new \Encore\CesensBlog\Model\SerieGrafica($ubicacion, $this->metrica);
                $ultimoDato = $this->grafica->ultimoDato($serie);
                if ($ultimoDato !== null) {
                    $html = self::htmlMetrica(
                        $this->metrica->getNombre(),
                        $ultimoDato->getValor(),
                        $this->metrica->getUnidad(),
                        0
                    );
                } else {
                    $html = '';
                }
                foreach ($this->informe->getAnualidadesAnteriores() as $año) {
                    $ultimoDato = $this->grafica->ultimoDato($serie, $año);
                    if ($ultimoDato !== null) {
                        $html .= self::htmlMetrica(
                            $this->metrica->getNombre() . ' ' . $año,
                            $ultimoDato->getValor(),
                            $this->metrica->getUnidad(),
                            0
                        );
                    }
                }
                return self::htmlGrupo($ubicacion->getNombre(), $html);
            } else {
                return null;
            }
        }
        switch ($this->metrica->getGrupo()) {
            case 1:// temperatura
                return $this->getValoresTemperatura($ubicacion);
            case 2:// humedad
                return $this->getValoresHumedad($ubicacion);
            case 3:// suelo
                break;
            case 4:// viento y presión
                break;
            case 5:// precipitaciones
                return $this->getValoresPrecipitaciones($ubicacion);
        }
        return null;
    }

    private function getValoresTemperatura(\Encore\CesensBlog\Model\Ubicacion $ubicacion)
    {
        $sql = <<<SQL
SELECT MIN(valor) AS minima, MAX(valor) AS maxima, AVG(valor) AS media
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
SQL;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
            $ubicacion->getId(),
            $this->metrica->getId(),
            self::$AYER->format('Y-m-d 00:00:00'),
            self::$AYER->format('Y-m-d 23:59:59'),
        ]);
        $t = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (empty($t)) {
            return null;
        }
        $metricas = '';
        if (!empty($t['media'])) {
            $metricas .= self::htmlMetrica('Media ayer:', $t['media'], $this->metrica->getUnidad());
        }
        if (!empty($t['minima'])) {
            $metricas .= self::htmlMetrica('Mínima ayer:', $t['minima'], $this->metrica->getUnidad());
        }
        if (!empty($t['maxima'])) {
            $metricas .= self::htmlMetrica('Máxima ayer:', $t['maxima'], $this->metrica->getUnidad());
        }
        return empty($metricas) ? null : self::htmlGrupo($ubicacion->getNombre(), $metricas);
    }

    private function getValoresHumedad(\Encore\CesensBlog\Model\Ubicacion $ubicacion)
    {
        $muestreo = 60 / ($ubicacion->muestreo() ?: 60);
        $metricas = '';
        $metrica = \Encore\CesensBlog\Model\Metrica::create([
            'id' => \Encore\CesensBlog\Model\Metrica::$LISTA['h'],
        ]);
        if ($ubicacion->metricaDisponible($metrica)) {
            $sql = <<<SQL
SELECT COUNT(*) - 1 AS horasHumectacion
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
  AND valor >= 10
HAVING COUNT(*) > 1
SQL;
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                $ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['h'],
                self::$AYER->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $h = $stmt->fetch(\PDO::FETCH_ASSOC);
            $h = $h['horasHumectacion'] / $muestreo;
            $metricas .= self::htmlMetrica('Humectación >10% ayer:', $h, 'h');
            $dias = $this->informe->getDias() ?: 7;
            $desde = new \DateTime('now');
            $desde->modify('-' . $dias . ' days');
            $stmt->execute([
                $ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['h'],
                $desde->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $h = $stmt->fetch(\PDO::FETCH_ASSOC);
            $h = $h['horasHumectacion'] / $muestreo;
            if (!empty($h)) {
                $metricas .= self::htmlMetrica('Humectación >10% en los últimos ' . $dias . ' días:', $h, 'h');
            }
        }
        $metrica = \Encore\CesensBlog\Model\Metrica::create([
            'id' => \Encore\CesensBlog\Model\Metrica::$LISTA['H'],
        ]);
        if ($ubicacion->metricaDisponible($metrica)) {
            $sql = <<<SQL
SELECT AVG(valor) AS media
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
SQL;
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                $ubicacion->getId(),
                \Encore\CesensBlog\Model\Metrica::$LISTA['H'],
                self::$AYER->format('Y-m-d 00:00:00'),
                self::$AYER->format('Y-m-d 23:59:59'),
            ]);
            $h = $stmt->fetch(\PDO::FETCH_ASSOC);
            if (!empty($h) && !empty($h['media'])) {
                $metricas .= self::htmlMetrica('Humedad media ayer:', $h['media'], '%');
            }
        }
        return empty($metricas) ? null : self::htmlGrupo('Humedad', $metricas);
    }

    private function getValoresPrecipitaciones(\Encore\CesensBlog\Model\Ubicacion $ubicacion)
    {
        $metricas = '';
        $sql = <<<SQL
SELECT SUM(valor) AS precip
FROM Dato
WHERE ubicacion = ?
  AND metrica = ?
  AND fecha BETWEEN ? AND ?
SQL;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
            $ubicacion->getId(),
            \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
            self::$AYER->format('Y-m-d 00:00:00'),
            self::$AYER->format('Y-m-d 23:59:59'),
        ]);
        $p = $stmt->fetch(\PDO::FETCH_ASSOC);
        $metricas .= self::htmlMetrica('Ayer:', $p['precip'],  'l/m²');
        $dias = $this->informe->getDias() ?: 7;
        $desde = new \DateTime('now');
        $desde->modify('-' . $dias . ' days');
        $stmt->execute([
            $ubicacion->getId(),
            \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
            $desde->format('Y-m-d 00:00:00'),
            self::$AYER->format('Y-m-d 23:59:59'),
        ]);
        $p = $stmt->fetch(\PDO::FETCH_ASSOC);
        $metricas .= self::htmlMetrica('Últimos ' . $dias . ' días:', $p['precip'], 'l/m²');
        $desde = new \DateTime('now');
        $desde->modify('-30 days');
        $stmt->execute([
            $ubicacion->getId(),
            \Encore\CesensBlog\Model\Metrica::$LISTA['Plu'],
            $desde->format('Y-m-d 00:00:00'),
            self::$AYER->format('Y-m-d 23:59:59'),
        ]);
        $p = $stmt->fetch(\PDO::FETCH_ASSOC);
        $metricas .= self::htmlMetrica('Últimos 30 días:', $p['precip'], 'l/m²');
        return self::htmlGrupo('Precipitaciones', $metricas);
    }
}
