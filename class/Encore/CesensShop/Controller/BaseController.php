<?php

namespace Encore\CesensShop\Controller;

use Encore\Application\Controller\AbstractActionController;
use Encore\Application\Controller\Exception\BadRequestException;
use Encore\Application\Controller\Exception\ForbiddenException;
use Encore\Application\Controller\Exception\NotFoundException;
use Encore\Application\Controller\Parameters\SingleModulePathRestParameters;
use Encore\Application\Module\Module;
use Encore\CesensShop\Model\User;
use PDO;

abstract class BaseController extends AbstractActionController
{
    public static $INSTANCE = null;

    private static  $user = null;

    private $pdo;

    public function __construct(Module $module)
    {
        parent::__construct($module);
        self::$INSTANCE = $this;
    }

    public function getPdo()
    {
        if ($this->pdo === null) {
            $params = $this->getModule()->getConfig('database');
            if ($params === null) {
                throw new \Exception('No database connection parameters');
            }
            $this->pdo = new PDO(
                $params['dsn'],
                $params['user'],
                $params['passwd'],
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
                ]
            );
        }
        return $this->pdo;
    }

    public function actionOptions()
    {
        $methods = ['HEAD', 'GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'TRACE', 'CONNECT', 'PATCH'];
        $allowed = [];
        foreach ($methods as $method) {
            if (method_exists($this, 'action' . ucfirst(strtolower($method)))) {
                $allowed[] = $method;
            }
        }
        if (!empty($allowed)) {
            header('Allow: ' . implode(',', $allowed));
        }
    }

    protected function getInput()
    {
        return file_get_contents('php://input');
    }

    protected function getJson()
    {
        $json = json_decode($this->getInput(), JSON_OBJECT_AS_ARRAY);
        if (!is_array($json)) {
            throw new BadRequestException(_('Invalid JSON'));
        }
        return $json;
    }

    protected function returnObject($data)
    {
        return $this->view($this->getViewFromFormat(), [
            'object' => $data,
        ]);
    }

    protected function formatFromMimeType($mime)
    {
        static $list = [
            'text/plain' => 'txt',
        ];
        $mime = explode(';', $mime, 2)[0];// eliminar el charset, si se recibe
        if (isset($list[$mime])) {
            return $list[$mime];
        } else {
            $pieces = explode('/', $mime, 2);
            return array_pop($pieces);
        }
    }

    public function getViewFromFormat($default = 'json')
    {
        $valid = [
            'application/json' => 'json',
            //'application/xml'  => 'xml',
            'text/plain'        => 'txt',
            'text/cesens-frame' => 'cesens-frame',
        ];
        $format = $this->getGet('format');
        $view = in_array($format, $valid) ? $format : null;
        if ($view === null && isset($_SERVER['HTTP_ACCEPT'])) {
            $list = [];
            foreach (explode(',', $_SERVER['HTTP_ACCEPT']) as $accept) {
                $accept = explode(';', $accept);
                $factor = 1.0;
                if (count($accept) > 1) {
                    $factor = (float)explode('=', $accept[1], 2)[1];
                }
                $list[$accept[0]] = $factor;
            }
            arsort($list, SORT_NUMERIC);
            foreach ($list as $accept => $factor) {
                if (isset($valid[$accept])) {
                    $view = $valid[$accept];
                    break;
                }
            }
        }
        return '/' . ($view ?: $default) . '.php';
    }

    public function getUriParams()
    {
        /** @var SingleModulePathRestParameters $params */
        $params = $this->getApplication()->getControllerParameters();
        return $params->getUriParams();
    }

    public static function getUser()
    {
        if (empty(self::$user) || self::$user == null ) {
            throw new ForbiddenException('Authentication required');
        } else{
            return self::$user;
        }
    }

    public static function setUser(User $u)
    {
        if (empty($u)) {
            throw new ForbiddenException('Authentication required');
        } else{
            self::$user = $u;
        }
    }

    public function getVersionNodo()
    {
        static $versionNodo = 0;
        if ($versionNodo === 0) {
            $versionNodo = null;
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                $versionNodo = VersionNodo::parseUserAgent($_SERVER['HTTP_USER_AGENT']);
            }
        }
        return $versionNodo;
    }

    public function tienePermisoAdmin()
    {
        $usuario = $this->getUsuario();
        return $usuario !== null && $usuario->getAdministrador();
    }

    /**
     * @param int $length Longitud de la cadena aleatoria a generar
     * @return string
     */
    protected static function randomString($length)
    {
        $string = '';
        static $keys = null;
        if ($keys === null) {
            $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        }
        for ($i = 0; $i < $length; $i++) {
            $string .= $keys[array_rand($keys)];
        }
        return $string;
    }

    public static function getAppVersion()
    {
        $key = 'HTTP_X_APP_VERSION';
        return isset($_SERVER[$key]) ? $_SERVER[$key] : null;
    }

    protected function procesarArchivo(Archivo $archivo, array $data)
    {
        $archivo->setUsuario($this->getUsuario());
        $datos = null;
        if (preg_match('`^data:(image/[^;]+);base64,`', $data['datos'], $matches) && count($matches) === 2) {
            $datos = base64_decode(substr($data['datos'], strlen($matches[0])));
        }
        if (empty($datos)) {
            throw new NotFoundException('El archivo no contiene datos');
        }
        $archivo->setNombre($data['nombre'] ?: '');
        $archivo->setTipo($data['tipo'] ?: $matches[1]);
        $archivo->setLongitud(strlen($datos));
        $archivo->save($this->baseDirArchivos(), $datos);
    }

    protected function baseDirArchivos()
    {
        return $this->getApplication()->getConfig('archivos');
    }
}
