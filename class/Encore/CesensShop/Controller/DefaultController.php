<?php

namespace Encore\CesensShop\Controller;

use Encore\Application\Controller\Exception\NotFoundException;

class DefaultController extends BaseController
{
    public function actionAction()
    {
        throw new NotFoundException(_('No encontrado'));
    }
}
