<?php


namespace Encore\CesensShop\Controller;



use Encore\Application\Controller\Exception\BadRequestException;
use Encore\Application\Controller\Exception\NotFoundException;
use Encore\CesensShop\Model\Db\EstacionDbEn;
use Encore\CesensShop\Model\EstacionEn;

class EstacionesEnController extends BaseController
{
    public function check(EstacionDbEn $estacionDb, $params) {
        $estacion = $estacionDb->get($params[1]);

        if(empty($estacion)){
            throw new NotFoundException('This station does not exist');
        }
        return $estacion;
    }

    public function actionGet()
    {
        $estacionDb_en = new EstacionDbEn($this->getPdo());
        $params = $this->getUriParams();
        parse_str($_SERVER['QUERY_STRING'], $query);
        $cParams = count($params);

        switch ($cParams) {
            case 0:
                $all = $estacionDb_en->listAll();

                if (sizeof($all) === 0) {
                    throw new NotFoundException('Nothing to show');
                }
                return $this->returnObject($all);
            case 1:
                switch ($params[0]){
                    default:
                        $estacion = $estacionDb_en->get($params[0]);
                        if (empty($estacion)){
                            throw new NotFoundException('This station does not exist');
                        }
                        return $this->returnObject($estacion);
                }

            case 2:
                switch ($params[1]){

                    default:
                        if($params[0] == 'es'){
                            $estacion = $estacionDb_en->getSpanishStation($params[1]);
                            if (empty($estacion)){
                                throw new NotFoundException('This station does not have spanish translation');
                            }
                            return $this->returnObject($estacion);
                        } else{
                            $estacion = $estacionDb_en->getOneEstacionAllSensors($params[1]);
                            if (empty($estacion)){
                                throw new NotFoundException('This station does not have sensors');
                            }
                            return $this->returnObject($estacion);
                        }
                }

        }
    }

    public function actionPost(){
        $cdb = new EstacionDbEn($this->getPdo());
        $estacionDb = new EstacionDbEn($this->getPdo());
        $params = $this->getUriParams();
        $cParams = count($params);

        switch ($cParams){
            case 0:
                $json = $this->getJson();
                $estacion = EstacionEn::create($json);

                $estacion->setId(null);

                if(empty($estacion->getName())){
                    throw new NotFoundException('the name of the stttation can\'t be empty');
                }
                $estacion->getPrice();
                $estacion->getDescription();
                $estacion->getPhoto();
                $estacion->getGarantia();
                $estacion->getSoftware();
                $estacion->getEstacionId();

                $cdb->save($estacion);

                return $this->returnObject($estacion);

            case 2:
                switch ($params[1]){
                    default:
                        $test = $estacionDb->addSensorToStation($params[0], $params[1]);
                        if (empty($test)){
                            throw new NotFoundException('This station does not exist');
                        }
                        return $this->returnObject($test);
                }
        }
    }

    public function actionPut() {
        $params = $this->getUriParams();
        $cParams = count($params);

        if ($params[0] !== 'edit') {
            throw new BadRequestException(_('Please add /edit/:id to your path'));
        }

        $cdb = new EstacionDbEn($this->getPdo());
        switch ($cParams) {
            case 2:
                $oldStation = $this->check($cdb,$params);

                $json = $this->getJson();
                $newStation = EstacionEn::create($json);

                $result = $cdb->update($newStation,$oldStation, $params[1]);

                return $this->returnObject([
                    'msg' => _('station has been modified succefuly'),
                ]);

            case 3:
                $oldStation = $this->check($cdb,$params);

                $json = $this->getJson();
                $newStation = EstacionEn::create($json);

                $result = $cdb->addSpanishStationId($oldStation->getId(), $params[2]);

                return $this->returnObject([
                    'msg' => _('station in spanish has been added succefuly'),
                ]);
        }

    }

    public function actionDelete() {

        $params = $this->getUriParams();
        if (count($params) !== 2) {
            throw new BadRequestException(_('Incorrect parameter number'));
        }
        if ($params[0] !== 'delete') {
            throw new BadRequestException(_('Incorrect parameter number'));
        }
        $es = new EstacionDbEn($this->getPdo());

        // check if the station exists in the database
        $this->check($es,$params);

        //delete the station from database
        $es->delete($params[1]);
        return $this->returnObject([
            'msg' => _('station has been deleted'),
        ]);

    }


}