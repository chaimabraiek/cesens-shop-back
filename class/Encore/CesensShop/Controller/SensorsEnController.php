<?php


namespace Encore\CesensShop\Controller;


use Encore\Application\Controller\Exception\BadRequestException;
use Encore\Application\Controller\Exception\NotFoundException;
use Encore\CesensShop\Model\Db\SensorDbEn;
use Encore\CesensShop\Model\SensorEn;

class SensorsEnController extends BaseController
{
    public function check(SensorDbEn $sensorDb, $params) {
        $sensor = $sensorDb->get($params[1]);

        if(empty($sensor)){
            throw new NotFoundException('This sensor does not exist');
        }
        return $sensor;
    }

    public function actionGet()
    {
        $estacionDb = new SensorDbEn($this->getPdo());
        $params = $this->getUriParams();
        parse_str($_SERVER['QUERY_STRING'], $query);
        $cParams = count($params);

        switch ($cParams) {
            case 0:
                $all = $estacionDb->listAll();

                if (sizeof($all) === 0) {
                    throw new NotFoundException('Nothing to show');
                }
                return $this->returnObject($all);
            case 1:
                switch ($params[0]){
                    default:
                        $estacion = $estacionDb->get($params[0]);
                        if (empty($estacion)){
                            throw new NotFoundException('This sensor does not exist');
                        }
                        return $this->returnObject($estacion);
                }
        }
    }

    public function actionPost(){
        $cdb = new SensorDbEn($this->getPdo());
        $params = $this->getUriParams();
        $cParams = count($params);

        switch ($cParams){
            case 0:
                $json = $this->getJson();
                $sensor = SensorEn::create($json);

                $sensor->setId(null);

                if(empty($sensor->getName())){
                    throw new NotFoundException('the name of the sensor can\'t be empty');
                }
                $sensor->getType();
                $sensor->getDescription();
                $sensor->getQuantity();
                $sensor->getPhoto();
                $sensor->getSensorEsId();
                $cdb->save($sensor);

                return $this->returnObject($sensor);
        }
    }

    public function actionPut() {

        $params = $this->getUriParams();
        $cParams = count($params);

        if ($params[0] !== 'edit') {
            throw new BadRequestException(_('Please add /edit/:id to your path'));
        }

        $cdb = new SensorDbEn($this->getPdo());
        switch ($cParams) {
            case 2:
                $oldSensor = $this->check($cdb,$params);

                $json = $this->getJson();
                $newSensor = SensorEn::create($json);

                $result = $cdb->update($newSensor,$oldSensor, $params[1]);

                return $this->returnObject([
                    'msg' => _('sensor has been modified succefuly'),
                ]);

            case 3:
                $oldSensor = $this->check($cdb,$params);

                $json = $this->getJson();
                $newSensor = SensorEn::create($json);

                $result = $cdb->addSpanishSensorId($oldSensor->getId(), $params[2]);

                return $this->returnObject([
                    'msg' => _('sensor in spanish has been added succefuly'),
                ]);
        }

    }

}