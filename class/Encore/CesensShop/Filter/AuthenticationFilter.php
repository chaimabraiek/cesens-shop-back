<?php

namespace Encore\CesensShop\Filter;

use Encore\Application\Controller\Exception\Exception;
use Encore\Application\Controller\Exception\ForbiddenException;
use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;
use Encore\CesensShop\Controller\BaseController;
use Encore\CesensShop\Model\User;
use Nowakowskir\JWT\Exceptions\IntegrityViolationException;
use Nowakowskir\JWT\Exceptions\TokenExpiredException;
use Nowakowskir\JWT\Exceptions\TokenInactiveException;
use Nowakowskir\JWT\JWT;
use Nowakowskir\JWT\TokenEncoded;

class AuthenticationFilter extends AbstractFilter
{
    private static $PUBLICO = [

        'estaciones' => [
            'get'    => true,
            'post'   => true,
            'delete'   => true,
            'put'   => true,
        ],
        'estaciones_en' => [
            'get'    => true,
            'post'   => true,
            'delete'   => true,
            'put'   => true,
        ],
        'sensors' => [
            'get'    => true,
            'post'   => true,
            'delete'   => true,
            'put'   => true,
        ],
        'sensors_en' => [
            'get'    => true,
            'post'   => true,
            'delete'   => true,
            'put'   => true,
        ],
    ];

    /**
     * Get header Authorization
     * */
    function getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        }else throw new ForbiddenException(_('Authentication Required'));
        return $headers;
    }

    /**
     * get access token from header
     * */
    function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    public function filter(FilterStack $stack)
    {
        $params = $stack->getApplication()->getControllerParameters();
        $controlador = $params->getController();
        $accion = strtolower(substr($stack->getActionMethod(), 6));
        if ($accion === 'options' || !$this->esAreaPrivada($controlador, $accion)) {
            return $stack->next();
        }

        /** @var BaseController $ctrl */
        $ctrl = $stack->getController();
        $token = $this->getBearerToken();

        // TODO: las llamadas en el área 'pública' no actualizan el tiempo
        if ($token !== null) {
            $dirFile = $ctrl->getApplication()->getConfig('private');
            $publicKey = file_get_contents($dirFile.'/public.pub');
            $tokenEncoded = new TokenEncoded($token);

            try {
                $tokenEncoded->validate($publicKey, JWT::ALGORITHM_RS256);
            } catch (IntegrityViolationException $e) {
                // Token is not trusted
                throw new ForbiddenException(_('Authentication Required'));
            } catch(TokenExpiredException $e) {
                // Token expired (exp date reached)
                throw new ForbiddenException(_('Authentication Required'));
            } catch(TokenInactiveException $e) {
                // Token is not yet active (nbf date not reached)
                throw new ForbiddenException(_('Authentication Required'));
            } catch(Exception $e) {
                // Something else gone wrong
                throw new ForbiddenException(_('Authentication Required'));
            }

            // get user info
            $tokenDecoded = $tokenEncoded->decode();
            $payload = $tokenDecoded->getPayload();

            // set user
            $user = new User();
            $user->setId($payload['id']);
            $user->setEmail($payload['email']);
            $ctrl::setUser($user);

            return $stack->next();
        }
        throw new ForbiddenException(_('Autenticación requerida'));
    }

    protected function esAreaPrivada($controlador, $accion)
    {
        if (isset(self::$PUBLICO[$controlador][$accion])) {
            $params = self::$PUBLICO[$controlador][$accion];
            if (is_string($params)) {
                $params = [$params];
            }
            if (is_array($params)) {
                $uri = $_SERVER['REQUEST_URI'];
                foreach ($params as $param) {
                    if (isset($_GET[$param])) {// parámetro GET
                        return false;
                    }
                    if (preg_match('/\b' . $param . '\b/', $uri)) {// parámetro en URL
                        return false;
                    }
                }
            } else {
                return !$params;
            }
        }
        return true;
    }

}