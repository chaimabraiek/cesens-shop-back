<?php

namespace Encore\CesensShop\Filter;

use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;

class CorsFilter extends AbstractFilter
{
    public function filter(FilterStack $stack)
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
            header('Access-Control-Allow-Headers: Authorization, X-Language, Content-Type, X-App-Version');
        }
        return $stack->next();
    }
}
