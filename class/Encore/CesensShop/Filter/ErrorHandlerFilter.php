<?php

namespace Encore\CesensShop\Filter;

use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;

class ErrorHandlerFilter extends AbstractFilter
{
    public function __construct(array $params = [])
    {
        parent::__construct($params);
    }

    public function filter(FilterStack $stack)
    {
        $logDir = $stack->getApplication()->getDataPath() . '/log/error/';
        register_shutdown_function(function () use ($logDir) {
            $error = error_get_last();
            if (!empty($error) && $error['type'] === 1) {
                http_response_code(500);
                $string = date('Y-m-d H:i:s') . ' - Ha ocurrido algún error grave.' . PHP_EOL;
                $string .= 'Información del último error:' . PHP_EOL;
                $string .= print_r($error, true) . PHP_EOL;
                $url = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $string .= 'URL: ' . $url . PHP_EOL;
                $string .= 'Auth: ' . (isset($_SERVER['HTTP_AUTHENTICATION']) ? $_SERVER['HTTP_AUTHENTICATION'] : '-') . PHP_EOL;
                $string .= PHP_EOL . '===================================================================' . PHP_EOL . PHP_EOL;
                $logFile = $logDir . date('Ymd') . '.log';
                if (!file_exists($logFile)) {
                    @touch($logFile);
                }
                if (is_writable($logFile)) {
                    file_put_contents($logFile, $string, FILE_APPEND);
                }
            }
        });
        set_error_handler(function ($errno, $errstr, $errfile, $errline, $errcontext) use ($logDir) {
            if (!(error_reporting() & $errno)) {
                return false;
            }
            $trace = array_reverse(debug_backtrace());
            array_pop($trace);

            $errorTypes = [
                1 => 'fatal_error',
                2 => 'warning',
                4 => 'parse',
                8 => 'notice',
                16 => 'core_error',
                32 => 'core_warning',
                64 => 'compile_error',
                128 => 'compile_warning',
                256 => 'user_error',
                512 => 'user_warning',
                1024 => 'user_notice',
                2048 => 'strict',
                4096 => 'recoverable_error',
                8192 => 'deprecated',
                16384 => 'user_deprecated',
            ];
            $string = date('Y-m-d H:i:s') . ' - Tipo de error: ' . strtoupper($errorTypes[$errno]) . PHP_EOL;
            $string .= '  \'' . $errstr . '\' en ' . $errfile . ':' . $errline . PHP_EOL;
            $string .= '  Backtrace:' . PHP_EOL;
            $i = 1;
            foreach ($trace as $item) {
                $string .= '    ' . $i++ . '  ' . (isset($item['file']) ? $item['file'] : '[unknown file]') . ':' . (isset($item['line']) ? $item['line'] : '[unknown line]') . '  --> ' . $item['function'] . '()' . PHP_EOL;
            }
            $string .= PHP_EOL;
            $context = print_r($errcontext, true);
            if (!empty($context)) {
                $logContext = $logDir . 'context/' . date('Ymd_His') . '_' . substr(md5(microtime()), 0, 3) . '.data.gz';
                if (@file_put_contents($logContext, gzencode($context)) !== false) {
                    $string .= 'Contexto: ' . $logContext . PHP_EOL;
                }
            }
            $url = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $string .= 'URL: ' . $url . PHP_EOL;
            $string .= PHP_EOL . '===================================================================' . PHP_EOL . PHP_EOL;
            $logFile = $logDir . date('Ymd') . '.log';
            if (!file_exists($logFile)) {
                @touch($logFile);
            }
            return is_writable($logFile) && file_put_contents($logFile, $string, FILE_APPEND) !== false;
        });
        return $stack->next();
    }
}
