<?php

namespace Encore\CesensShop\Filter;

use Encore\Application\Controller\Exception\Exception;
use Encore\Application\Controller\Exception\InternalServerErrorException;
use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;
use Encore\Application\View\View;
use Encore\CesensShop\Controller\BaseController;

class ExceptionFilter extends AbstractFilter
{
    public function filter(FilterStack $stack)
    {
        try {
            return $stack->next();
        } catch (\Exception $e) {
            if (!($e instanceof Exception)) {
                $e = new InternalServerErrorException($e->getMessage(), $e->getCode(), $e);
            }
            /** @var BaseController $ctrl */
            $ctrl = $stack->getController();
            $e->setView(new View($stack->getApplication()->getViewPath($ctrl->getViewFromFormat()), [
                'object' => [
                    'msg' => $e->getMessage(),
                ],
            ]));
            throw $e;
        }
    }
}
