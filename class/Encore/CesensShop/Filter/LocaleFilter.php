<?php

namespace Encore\CesensShop\Filter;

use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;

class LocaleFilter extends AbstractFilter
{
    public function filter(FilterStack $stack)
    {
        $app = $stack->getApplication();
        $idiomas = $app->getConfig('idiomas');
        $idioma = isset($_SERVER['HTTP_X_LANGUAGE']) ? strtolower($_SERVER['HTTP_X_LANGUAGE']) : null;
        if ($idioma === null || !isset($idiomas[$idioma])) {
            $idioma = array_values($idiomas)[0];
        } else {
            $idioma = $idiomas[$idioma];
        }
        $idioma .= '.utf8';
        if (setlocale(LC_MESSAGES, $idioma) !== $idioma) {
            trigger_error('Error al cambiar de LOCALE "'. $idioma . '"', E_USER_NOTICE);
        }
        $domain = 'cesens';
        bindtextdomain($domain, $app->getDataPath('locale'));
        bind_textdomain_codeset($domain, 'UTF-8');
        textdomain($domain);

        setlocale(LC_TIME, 'es_ES');
        date_default_timezone_set('Europe/Madrid');
        return $stack->next();
    }
}
