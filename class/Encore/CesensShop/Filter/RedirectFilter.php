<?php

namespace Encore\CesensShop\Filter;

use Encore\Application\Controller\AbstractActionController;
use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;

class RedirectFilter extends AbstractFilter
{
    public function filter(FilterStack $stack)
    {
        if ($_SERVER['REQUEST_URI'] === '/') {
            /** @var AbstractActionController $ctrl */
            $ctrl = $stack->getController();
            return $ctrl->redirect('/app');
        } else {
            return $stack->next();
        }
    }
}