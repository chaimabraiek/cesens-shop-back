<?php


namespace Encore\CesensShop\Model\Db;


use Encore\Application\Model\ModelDb;
use Encore\CesensShop\Model\Estacion;
use Encore\CesensShop\Model\EstacionEn;

class EstacionDb extends ModelDb
{
    public function __construct(\PDO $pdo)
    {
        $this->setPdo($pdo);
    }

    public function countTable() {
        return $this->_count('Estacion');
    }

    public function get($id, $fillIn = true)
    {
        /** @var Estacion $estacion */
        $estacion = $this->getById('Estacion', $id, Estacion::class);
        return $estacion;
    }

    public function listAll(){
        return $this->getAllEntities('Estacion', Estacion::class);
    }

    public function save(Estacion $estacion)
    {
        $params = [
            'id'             => $estacion->getId(),
            'name'           => $estacion->getName(),
            'price'          => $estacion->getPrice(),
            'description'    => $estacion->getDescription(),
            'photo'          => $estacion->getPhoto(),
            'garantia'       => $estacion->getGarantia(),
            'software'       => $estacion->getSoftware(),
        ];
        $result = $this->_save('Estacion', $params);
        if (!$result) {
            throw new \Exception(_('An error occurred while saving the station'));
        }
        if (empty($estacion->getId())) {
            $estacion->setId($result);
        }
        return $result;
    }

    public function delete($id)
    {
        return parent::_delete('Estacion', ['id' => $id]);
    }

    public function update(Estacion $estacion,Estacion $oldEstacion, $id){
        $params = [
            'name'         => $estacion->getName() ? $estacion->getName() : $oldEstacion->getName(),
            'price'        => $estacion->getPrice() ? $estacion->getPrice() : $oldEstacion->getPrice(),
            'description'  => $estacion->getDescription() ? $estacion->getDescription() : $oldEstacion->getDescription(),
            'photo'        => $estacion->getPhoto() ? $estacion->getPhoto() : $oldEstacion->getPhoto(),
            'garantia'     => $estacion->getGarantia() ? $estacion->getGarantia() : $oldEstacion->getGarantia(),
            'software'     => $estacion->getSoftware() ? $estacion->getSoftware() : $oldEstacion->getSoftware()
        ];

        return parent::_update('Estacion', $id , $params);
    }

    public function getOneEstacionAllSensors($id)
    {
        $sql = 'SELECT sensor.id, sensor.name, sensor.type, sensor.description, sensor.quantity, sensor.photo
                FROM Sensor AS sensor
                JOIN Estacion_Sensor AS es ON ( es.sensor = sensor.id )
                JOIN Estacion AS estacion ON ( es.estacion = estacion.id )
                WHERE estacion.id  = ?
                LIMIT 0 , 30 ';
        return $this->select($sql, [$id]);
    }

    public function getEnglishStation($id){
        $sql = 'SELECT en.id , en.name, en.price, en.description , en.photo, en.garantia , en.software, en.estacion_id FROM `Estacion_En` as en 
                JOIN `Estacion` as es ON ( es.estacion_en_id = en.id ) 
                WHERE es.id = ?';
        return $this->getSingleEntity(EstacionEn::class,$sql, [$id]);
    }

    public function addSensorToStation($estacion, $sensor){
        $params = [
            'estacion'             => $estacion,
            'sensor'           => $sensor,
        ];
        $result = $this->_save('Estacion_Sensor', $params);
        if (!$result) {
            throw new \Exception(_('An error occurred while saving the station'));
        }

        return $result;
    }

    public function addEnglishStationId($estacion , $estacion_en){
        $params = [
            'estacion_en_id'             => $estacion_en
        ];
        return parent::_update('Estacion', $estacion , $params);
    }

}