<?php


namespace Encore\CesensShop\Model\Db;


use Encore\Application\Model\ModelDb;
use Encore\CesensShop\Model\Estacion;
use Encore\CesensShop\Model\EstacionEn;

class EstacionDbEn extends ModelDb
{
    public function __construct(\PDO $pdo)
    {
        $this->setPdo($pdo);
    }

    public function countTable() {
        return $this->_count('Estacion_En');
    }

    public function get($id, $fillIn = true)
    {
        /** @var EstacionEn $estacion */
        $estacion = $this->getById('Estacion_En', $id, EstacionEn::class);
        return $estacion;
    }

    public function listAll(){
        return $this->getAllEntities('Estacion_En', EstacionEn::class);
    }

    public function save(EstacionEn $estacion)
    {
        $params = [
            'id'             => $estacion->getId(),
            'name'           => $estacion->getName(),
            'price'          => $estacion->getPrice(),
            'description'    => $estacion->getDescription(),
            'photo'          => $estacion->getPhoto(),
            'garantia'       => $estacion->getGarantia(),
            'software'       => $estacion->getSoftware(),
            'estacion_id'    => $estacion->getEstacionId(),
        ];
        $result = $this->_save('Estacion_En', $params);
        if (!$result) {
            throw new \Exception(_('An error occurred while saving the station'));
        }
        if (empty($estacion->getId())) {
            $estacion->setId($result);
        }
        return $result;
    }

    public function delete($id)
    {
        return parent::_delete('Estacion_En', ['id' => $id]);
    }

    public function update(EstacionEn $estacion,EstacionEn $oldEstacion, $id){
        $params = [
            'name'         => $estacion->getName() ? $estacion->getName() : $oldEstacion->getName(),
            'price'        => $estacion->getPrice() ? $estacion->getPrice() : $oldEstacion->getPrice(),
            'description'  => $estacion->getDescription() ? $estacion->getDescription() : $oldEstacion->getDescription(),
            'photo'        => $estacion->getPhoto() ? $estacion->getPhoto() : $oldEstacion->getPhoto(),
            'garantia'     => $estacion->getGarantia() ? $estacion->getGarantia() : $oldEstacion->getGarantia(),
            'software'     => $estacion->getSoftware() ? $estacion->getSoftware() : $oldEstacion->getSoftware()
        ];

        return parent::_update('Estacion_En', $id , $params);
    }

    public function getOneEstacionAllSensors($id)
    {
        $sql = 'SELECT sensor.id, sensor.name, sensor.type, sensor.description, sensor.quantity, sensor.photo
                FROM Sensor_En AS sensor
                JOIN Estacion_Sensor_En AS es ON ( es.sensor_en = sensor.id )
                JOIN Estacion_En AS Estacion_En ON ( es.Estacion_En = Estacion_En.id )
                WHERE Estacion_En.id  = ?
LIMIT 0 , 30 ';
        return $this->select($sql, [$id]);
    }

    public function getSpanishStation($id){
        $sql = 'SELECT es.id , es.name, es.price, es.description , es.photo, es.garantia , es.software, es.estacion_en_id FROM `Estacion` as es 
                JOIN `Estacion_En` as en ON ( en.estacion_id = es.id ) 
                WHERE en.id = ?';
        return $this->getSingleEntity(Estacion::class,$sql, [$id]);
    }

    public function addSensorToStation($estacion, $sensor){
        $params = [
            'estacion_en'             => $estacion,
            'sensor_en'           => $sensor,
        ];
        $result = $this->_save('Estacion_Sensor_En', $params);
        if (!$result) {
            throw new \Exception(_('An error occurred while saving the station'));
        }

        return $result;
    }

    public function addSpanishStationId($estacion_en , $estacion_es){
        $params = [
            'estacion_id'             => $estacion_es
        ];
        return parent::_update('Estacion_En', $estacion_en , $params);
    }


}