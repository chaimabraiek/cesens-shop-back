<?php


namespace Encore\CesensShop\Model\Db;


use Encore\Application\Model\ModelDb;
use Encore\CesensShop\Model\Sensor;

class SensorDb extends ModelDb
{
    public function __construct(\PDO $pdo)
    {
        $this->setPdo($pdo);
    }

    public function countTable() {
        return $this->_count('Sensor');
    }

    public function get($id, $fillIn = true)
    {
        /** @var Sensor $sensor */
        $sensor = $this->getById('Sensor', $id, Sensor::class);
        return $sensor;
    }

    public function listAll(){
        return $this->getAllEntities('Sensor', Sensor::class);
    }

    public function save(Sensor $sensor)
    {
        $params = [
            'id'             => $sensor->getId(),
            'name'           => $sensor->getName(),
            'type'          => $sensor->getType(),
            'description'    => $sensor->getDescription(),
            'quantity'    => $sensor->getQuantity(),
            'photo'    => $sensor->getPhoto(),

        ];
        $result = $this->_save('Sensor', $params);
        if (!$result) {
            throw new \Exception(_('An error occurred while saving the sensor'));
        }
        if (empty($sensor->getId())) {
            $sensor->setId($result);
        }
        return $result;
    }

    public function delete($id)
    {
        return parent::_delete('Sensor', ['id' => $id]);
    }

    public function update(Sensor $sensor,Sensor $oldSensor, $id){
        $params = [
            'name'         => $sensor->getName() ? $sensor->getName() : $oldSensor->getName(),
            'type'        => $sensor->getType() ? $sensor->getType() : $oldSensor->getType(),
            'description'  => $sensor->getDescription() ? $sensor->getDescription() : $oldSensor->getDescription(),
            'quantity'        => $sensor->getQuantity() ? $sensor->getQuantity() : $oldSensor->getQuantity(),
        ];

        return parent::_update('Sensor', $id , $params);
    }

    public function addEnglishSensorId($sensor_es , $sensor_en){
        $params = [
            'sensor_en_id'             => $sensor_en
        ];
        return parent::_update('Sensor', $sensor_es , $params);
    }

    public function getEnglishSensor($id){
        $sql = 'SELECT en.id , en.name, en.type, en.description , en.quantity, en.photo, en.sensor_es_id FROM `Sensor_En` as en 
                JOIN `Sensor` as es ON ( es.sensor_en_id = en.id ) 
                WHERE es.id = ?';
        return $this->getSingleEntity(Sensor::class,$sql, [$id]);
    }

}