<?php


namespace Encore\CesensShop\Model\Db;


use Encore\Application\Model\ModelDb;
use Encore\CesensShop\Model\SensorEn;

class SensorDbEn extends ModelDb
{
    public function __construct(\PDO $pdo)
    {
        $this->setPdo($pdo);
    }

    public function countTable() {
        return $this->_count('Sensor_En');
    }

    public function get($id, $fillIn = true)
    {
        /** @var SensorEn $sensor */
        $sensor = $this->getById('Sensor_En', $id, SensorEn::class);
        return $sensor;
    }

    public function listAll(){
        return $this->getAllEntities('Sensor_En', SensorEn::class);
    }

    public function save(SensorEn $sensor)
    {
        $params = [
            'id'             => $sensor->getId(),
            'name'           => $sensor->getName(),
            'type'           => $sensor->getType(),
            'description'    => $sensor->getDescription(),
            'quantity'       => $sensor->getQuantity(),
            'photo'       => $sensor->getPhoto(),
            'sensor_es_id'   => $sensor->getSensorEsId()

        ];
        $result = $this->_save('Sensor_En', $params);
        if (!$result) {
            throw new \Exception(_('An error occurred while saving the SensorEn'));
        }
        if (empty($sensor->getId())) {
            $sensor->setId($result);
        }
        return $result;
    }

    public function delete($id)
    {
        return parent::_delete('Sensor_En', ['id' => $id]);
    }

    public function update(SensorEn $sensor,SensorEn $oldSensor, $id){
        $params = [
            'name'         => $sensor->getName() ? $sensor->getName() : $oldSensor->getName(),
            'type'         => $sensor->getType() ? $sensor->getType() : $oldSensor->getType(),
            'description'  => $sensor->getDescription() ? $sensor->getDescription() : $oldSensor->getDescription(),
            'quantity'     => $sensor->getQuantity() ? $sensor->getQuantity() : $oldSensor->getQuantity(),
        ];

        return parent::_update('Sensor_En', $id , $params);
    }

    public function addSpanishSensorId($sensor_en , $sensor_es){
        $params = [
            'sensor_es_id'             => $sensor_es
        ];
        return parent::_update('Sensor_En', $sensor_en , $params);
    }

}