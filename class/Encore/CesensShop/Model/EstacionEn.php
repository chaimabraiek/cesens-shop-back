<?php


namespace Encore\CesensShop\Model;


class EstacionEn extends Model
{
    private $id;
    private $name;
    private $price;
    private $description;
    private $photo;
    private $garantia;
    private $software;

    public $estacion_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return EstacionEn
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return EstacionEn
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return EstacionEn
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return EstacionEn
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return EstacionEn
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGarantia()
    {
        return $this->garantia;
    }

    /**
     * @param mixed $garantia
     * @return EstacionEn
     */
    public function setGarantia($garantia)
    {
        $this->garantia = $garantia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSoftware()
    {
        return $this->software;
    }

    /**
     * @param mixed $software
     * @return EstacionEn
     */
    public function setSoftware($software)
    {
        $this->software = $software;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstacionId()
    {
        return $this->estacion_id;
    }

    /**
     * @param mixed $estacion_id
     * @return EstacionEn
     */
    public function setEstacionId($estacion_id)
    {
        $this->estacion_id = $estacion_id;
        return $this;
    }



}