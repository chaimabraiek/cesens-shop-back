<?php

namespace Encore\CesensShop\Model;

use JsonSerializable;

abstract class Model extends \Encore\Application\Model\Model implements JsonSerializable
{
    /**
     * @var bool Establece si las instancias de esta clase se extraerán como ID
     */
    protected $modelAsId = true;
    abstract public function getId();

    public function extract($ignoreNulls = false, $modelAsId = null, $ignoreArrays = false)
    {
        $result  = [];
        $methods = get_class_methods($this);
        $modelAsId = $modelAsId !== null ? $modelAsId : $this->modelAsId;
        foreach ($methods as $method) {
            if (substr($method, 0, 3) === 'get') {
                $key = lcfirst(substr($method, 3));
                $value = call_user_func([$this, $method]);
                if (!$ignoreNulls || ($ignoreNulls && $value !== null)) {
                    if ($ignoreArrays && is_array($value)) {
                        continue;
                    }
                    if ($modelAsId && $value instanceof Model) {
                        $value = $value->getId();
                    }
                    $result[$key] = $value;
                }
            }
        }
        return $result;
    }

    public function populate(array $data)
    {
        unset($data['objectType']);
        return parent::populate($data);
    }

    public function jsonSerialize()
    {
        $array = $this->extract(false);
        $array['objectType'] = (new \ReflectionClass(get_class($this)))->getShortName();
        return $array;
    }

    protected static function parseFecha($fecha, $formato)
    {
        if ($fecha instanceof \DateTimeInterface) {
            return $fecha->format($formato);
        } elseif (!empty($fecha) && preg_match('/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:\.\d+)Z$/', $fecha, $parts) > 0) {
            $time = gmmktime($parts[4], $parts[5], $parts[6], $parts[2], $parts[3], $parts[1]);
            $inputTime = strtotime($fecha);
            if ($inputTime === false || $inputTime != $time) {
                return $fecha;
            } else {
                $f = \DateTime::createFromFormat('Y-m-d\TH:i:s+', $fecha, new \DateTimeZone('Etc/Zulu'));
                $f->setTimezone(new \DateTimeZone(date_default_timezone_get()));
                return $f->format($formato);
            }
        } else {
            return $fecha;
        }
    }

    /**
     * @param string|null $fecha
     * @param string|null $formato
     * @param string $formatoBase
     * @return \DateTime|string|null
     */
    protected static function formatoFecha($fecha, $formato = null, $formatoBase = 'Y-m-d|')
    {
        if ($formato === null || $fecha === null) {
            return $fecha;
        }
        $fecha = \DateTime::createFromFormat($formatoBase, $fecha);
        if ($fecha !== false) {
            return $formato === \DateTime::class ? $fecha : $fecha->format($formato);
        } else {
            return null;
        }
    }

    public function params(array $params)
    {
        $result = [];
        $extract = $this->extract(false, true, true);
        foreach ($params as $param) {
            if (isset($extract[$param])) {
                $result[$param] = $extract[$param];
            }
        }
        return $result;
    }

    public static function equals($a, $b)
    {
        if (is_object($a) && is_object($b)) {
            if (get_class($a) !== get_class($b)) {
                return false;
            }
        }
        $a = $a instanceof static ? $a->getId() : $a;
        $b = $b instanceof static ? $b->getId() : $b;
        return $a == $b;
    }
}
