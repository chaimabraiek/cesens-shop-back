<?php


namespace Encore\CesensShop\Model;


class SensorEn extends Model
{

    private $id;
    private $name;
    private $type;
    private $description;
    private $quantity;
    private $photo;
    private $sensor_es_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SensorEn
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return SensorEn
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return SensorEn
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return SensorEn
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return SensorEn
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getSensorEsId()
    {
        return $this->sensor_es_id;
    }

    /**
     * @param mixed $sensor_es_id
     * @return SensorEn
     */
    public function setSensorEsId($sensor_es_id)
    {
        $this->sensor_es_id = $sensor_es_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return SensorEn
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }


}