<?php


$include = function ($file, $default = null) {
    $path = __DIR__ . $file;
    if (is_readable($path)) {
        return include $path;
    } else {
        trigger_error('Falta archivo de configuración ' . $file, E_USER_WARNING);
        return $default;
    }
};

return [
    'version' => include __DIR__ . '/version.php',
    'executionParameters' => [
        'class' => \Encore\Application\Controller\Parameters\SingleModulePathRestParameters::class,
        'data' => [
            'basePath' => '/shop',
        ],
    ],
    'autoloaders' => [
        'formats' => [
            'path'      => '/vendor/encore/formats',
            'namespace' => 'Esncore\\Formats',
        ],
    ],
    'filterStack' => [
        'error' => [
            'class' => \Encore\CesensShop\Filter\ErrorHandlerFilter::class,
        ],
        'exception' => [
            'class' => \Encore\CesensShop\Filter\ExceptionFilter::class,
        ],
        'cors' => [
            'class' => \Encore\CesensShop\Filter\CorsFilter::class,
        ],
        'authentication' => [
            'class' => \Encore\CesensShop\Filter\AuthenticationFilter::class,
        ]
    ],
    'controllers' => [
        'estaciones'        => \Encore\CesensShop\Controller\EstacionesController::class,
        'estaciones_en'        => \Encore\CesensShop\Controller\EstacionesEnController::class,


        'sensors'        => \Encore\CesensShop\Controller\SensorsController::class,
        'sensors_en'        => \Encore\CesensShop\Controller\SensorsEnController::class,
    ],
    'database'  => include __DIR__ . '/bd.php',
    'private' => dirname(__DIR__) . '/data/private',
];
