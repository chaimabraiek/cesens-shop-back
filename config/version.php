<?php

return [
    'number' => '1.1.0-alpha',
    'label'  => '1.1 (alpha)',
    'final'  => false,
];
