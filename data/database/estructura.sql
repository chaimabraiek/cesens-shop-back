SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `Acceso`;
CREATE TABLE `Acceso` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` int(8) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `token` char(32) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `tiempo` int(6) unsigned NOT NULL DEFAULT 0,
  `ip` varchar(48) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `tipo` int(8) unsigned NOT NULL,
  `version` varchar(16) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_fecha` (`usuario`,`fecha`),
  UNIQUE KEY `token` (`token`),
  KEY `tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Alerta`;
CREATE TABLE `Alerta` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` int(8) unsigned DEFAULT NULL,
  `nombre` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `activar` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `prediccion` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `periodicidad` int(3) unsigned DEFAULT NULL COMMENT 'Horas (si es NULL una sola vez)',
  `todasUbicaciones` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `enviada` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'Indica si esta alerta ha sido enviada alguna vez',
  `predefinida` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_nombre` (`usuario`,`nombre`),
  KEY `activar` (`activar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `AlertaEnvio`;
CREATE TABLE `AlertaEnvio` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `alerta` int(8) unsigned NOT NULL,
  `ubicacion` int(8) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `grafica` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alerta_ubicacion_fecha` (`alerta`,`ubicacion`,`fecha`),
  KEY `ubicacion` (`ubicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `AlertaEnvioRegla`;
CREATE TABLE `AlertaEnvioRegla` (
  `alertaEnvio` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `operador` enum('>','<') COLLATE utf8_unicode_ci NOT NULL,
  `umbral` decimal(10,2) NOT NULL,
  PRIMARY KEY (`alertaEnvio`,`metrica`,`operador`,`umbral`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `AlertaHttpPost`;
CREATE TABLE `AlertaHttpPost` (
  `cliente` int(8) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Alerta_Ubicacion`;
CREATE TABLE `Alerta_Ubicacion` (
  `alerta` int(8) unsigned NOT NULL,
  `ubicacion` int(8) unsigned NOT NULL,
  PRIMARY KEY (`alerta`,`ubicacion`),
  KEY `ubicacion` (`ubicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Archivo`;
CREATE TABLE `Archivo` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` int(8) unsigned NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitud` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Autorizacion`;
CREATE TABLE `Autorizacion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `token` char(8) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `usuario` int(8) unsigned NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `todasUbicaciones` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  UNIQUE KEY `usuario_nombre` (`usuario`,`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Autorizacion_UbicacionCliente`;
CREATE TABLE `Autorizacion_UbicacionCliente` (
  `autorizacion` int(8) unsigned NOT NULL,
  `ubicacionCliente` int(8) unsigned NOT NULL,
  PRIMARY KEY (`autorizacion`,`ubicacionCliente`),
  KEY `ubicacionCliente` (`ubicacionCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Autorizacion_Usuario`;
CREATE TABLE `Autorizacion_Usuario` (
  `autorizacion` int(8) unsigned NOT NULL,
  `usuario` int(8) unsigned NOT NULL,
  PRIMARY KEY (`autorizacion`,`usuario`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Cliente`;
CREATE TABLE `Cliente` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ClienteAdcon`;
CREATE TABLE `ClienteAdcon` (
  `cliente` int(8) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ClienteAdcon_Metrica`;
CREATE TABLE `ClienteAdcon_Metrica` (
  `clienteAdcon` int(8) unsigned NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`clienteAdcon`,`nombre`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ClienteAgZoom`;
CREATE TABLE `ClienteAgZoom` (
  `cliente` int(8) unsigned NOT NULL,
  `usuario` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `clave` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ClienteBynse`;
CREATE TABLE `ClienteBynse` (
  `cliente` int(8) unsigned NOT NULL,
  `token` varchar(1024) NOT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `ClienteDecagon`;
CREATE TABLE `ClienteDecagon` (
  `cliente` int(8) unsigned NOT NULL,
  `email` varchar(128) NOT NULL,
  `userpass` varchar(64) NOT NULL,
  `url` varchar(512) NOT NULL COMMENT 'Aunque en Decagon den una URL para cada cliente, usar siempre la de Cesens (todas las URL son alias que usan para bloquear usuarios si se produce un abuso o algo va mal)',
  PRIMARY KEY (`cliente`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ClientePessl`;
CREATE TABLE `ClientePessl` (
  `cliente` int(8) unsigned NOT NULL,
  `publicKey` varchar(48) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `privateKey` varchar(48) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `url` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ClienteRanchSystems`;
CREATE TABLE `ClienteRanchSystems` (
  `cliente` int(8) unsigned NOT NULL,
  `usuario` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ClienteSico`;
CREATE TABLE `ClienteSico` (
  `cliente` int(8) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ComoConociste`;
CREATE TABLE `ComoConociste` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ComposicionSuelo`;
CREATE TABLE `ComposicionSuelo` (
  `ubicacionCliente` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `profundidad` int(4) unsigned NOT NULL,
  `arena` decimal(5,2) unsigned DEFAULT NULL,
  `limo` decimal(5,2) unsigned DEFAULT NULL,
  `arcilla` decimal(5,2) unsigned DEFAULT NULL,
  `elementosGruesos` decimal(5,2) unsigned DEFAULT NULL,
  `capacidadCampo` decimal(4,2) unsigned DEFAULT NULL,
  `puntoMarchitezPermanente` decimal(4,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`ubicacionCliente`,`metrica`),
  UNIQUE KEY `ubicacionCliente_profundidad` (`ubicacionCliente`,`profundidad`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ConectorSensor`;
CREATE TABLE `ConectorSensor` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nodo` int(8) unsigned NOT NULL,
  `sensor` int(8) unsigned NOT NULL,
  `orden` int(3) unsigned NOT NULL,
  `desde` datetime NOT NULL,
  `hasta` datetime DEFAULT NULL,
  `comentarios` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nodo_sensor_orden_desde` (`nodo`,`sensor`,`orden`,`desde`),
  KEY `sensor` (`sensor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ConectorSensor_ParametroCalibracion`;
CREATE TABLE `ConectorSensor_ParametroCalibracion` (
  `conectorSensor` int(8) unsigned NOT NULL,
  `parametroCalibracion` int(8) unsigned NOT NULL,
  `valor` decimal(12,5) unsigned NOT NULL,
  PRIMARY KEY (`conectorSensor`,`parametroCalibracion`),
  KEY `parametroCalibracion` (`parametroCalibracion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Consulta`;
CREATE TABLE `Consulta` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` int(8) unsigned NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `duracion` int(5) unsigned DEFAULT NULL,
  `desde` date DEFAULT NULL,
  `hasta` date DEFAULT NULL,
  `estados` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `prediccion` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_nombre` (`usuario`,`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ConsultaAnualidadAnterior`;
CREATE TABLE `ConsultaAnualidadAnterior` (
  `consulta` int(8) unsigned NOT NULL,
  `anualidad` year(4) NOT NULL,
  PRIMARY KEY (`consulta`,`anualidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Consulta_Metrica`;
CREATE TABLE `Consulta_Metrica` (
  `consulta` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `funcionAgrupacion` int(3) unsigned DEFAULT NULL,
  `intervaloAgrupacion` int(3) unsigned DEFAULT NULL,
  UNIQUE KEY `KEY` (`consulta`,`metrica`,`funcionAgrupacion`,`intervaloAgrupacion`),
  KEY `metrica` (`metrica`),
  KEY `funcionAgrupacion` (`funcionAgrupacion`),
  KEY `intervaloAgrupacion` (`intervaloAgrupacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Consulta_Ubicacion`;
CREATE TABLE `Consulta_Ubicacion` (
  `consulta` int(8) unsigned NOT NULL,
  `ubicacion` int(8) unsigned NOT NULL,
  PRIMARY KEY (`consulta`,`ubicacion`),
  KEY `ubicacion` (`ubicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Cultivo`;
CREATE TABLE `Cultivo` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Dato`;
CREATE TABLE `Dato` (
  `ubicacion` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  PRIMARY KEY (`ubicacion`,`metrica`,`fecha`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `DatoDerivado`;
CREATE TABLE `DatoDerivado` (
  `ubicacion` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  PRIMARY KEY (`ubicacion`,`metrica`,`fecha`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Email`;
CREATE TABLE `Email` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` int(8) unsigned NOT NULL,
  `fechaEnvio` datetime NOT NULL,
  `fechaLectura` datetime DEFAULT NULL,
  `ip` varchar(48) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Enfermedad`;
CREATE TABLE `Enfermedad` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `cultivo` int(8) unsigned NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` enum('enfermedad','plaga') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cultivo_nombre` (`cultivo`,`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Estadisticas`;
CREATE TABLE `Estadisticas` (
  `fecha` datetime NOT NULL,
  `datos` int(10) unsigned NOT NULL,
  `datosDerivados` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fecha`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `EstadisticasTotal`;
CREATE TABLE `EstadisticasTotal` (
  `fecha` datetime NOT NULL,
  `datos` int(10) unsigned NOT NULL,
  `datosDerivados` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fecha`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Estado`;
CREATE TABLE `Estado` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `cultivo` int(8) unsigned NOT NULL,
  `acronimo` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` char(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cultivo_nombre` (`cultivo`,`nombre`),
  UNIQUE KEY `cultivo_acronimo` (`cultivo`,`acronimo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `FuncionAgrupacion`;
CREATE TABLE `FuncionAgrupacion` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `acronimo` varchar(12) NOT NULL,
  `nombre` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acronimo` (`acronimo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `GraficaConsultada`;
CREATE TABLE `GraficaConsultada` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `acceso` int(8) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `estados` tinyint(1) unsigned NOT NULL,
  `prediccion` tinyint(1) unsigned NOT NULL,
  `seccionRiego` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acceso_fecha` (`acceso`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `GraficaConsultada_AnualidadAnterior`;
CREATE TABLE `GraficaConsultada_AnualidadAnterior` (
  `graficaConsultada` int(8) unsigned NOT NULL,
  `anualidadAnterior` int(4) unsigned NOT NULL,
  PRIMARY KEY (`graficaConsultada`,`anualidadAnterior`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `GraficaConsultada_Metrica`;
CREATE TABLE `GraficaConsultada_Metrica` (
  `graficaConsultada` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`graficaConsultada`,`metrica`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `GraficaConsultada_Ubicacion`;
CREATE TABLE `GraficaConsultada_Ubicacion` (
  `graficaConsultada` int(8) unsigned NOT NULL,
  `ubicacion` int(8) unsigned NOT NULL,
  PRIMARY KEY (`graficaConsultada`,`ubicacion`),
  KEY `ubicacion` (`ubicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `IncidenciaEnfermedad`;
CREATE TABLE `IncidenciaEnfermedad` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `localizacion` int(8) unsigned NOT NULL,
  `enfermedad` int(8) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `imagen` int(8) unsigned DEFAULT NULL,
  `certeza` tinyint(1) unsigned NOT NULL,
  `comentarios` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `_fechaModificacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `localizacion_enfermedad_fecha` (`localizacion`,`enfermedad`,`fecha`),
  KEY `enfermedad` (`enfermedad`),
  KEY `imagen` (`imagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `IncidenciaEnfermedadConteo`;
CREATE TABLE `IncidenciaEnfermedadConteo` (
  `incidenciaEnfermedad` int(8) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` int(8) unsigned NOT NULL,
  `imagen` int(8) unsigned DEFAULT NULL,
  `comentarios` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`incidenciaEnfermedad`,`fecha`),
  KEY `imagen` (`imagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Informe`;
CREATE TABLE `Informe` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `dias` int(4) unsigned DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(8) unsigned DEFAULT NULL,
  `variedad` int(8) unsigned DEFAULT NULL,
  `periodo` int(3) unsigned NOT NULL DEFAULT 1,
  `envio` int(3) unsigned NOT NULL,
  `agrupar` enum('nodo','metrica','ambos') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nodo',
  `fechaMetricasAcumulativas` date DEFAULT NULL COMMENT 'Fecha inicio a partir de la cual aplicar las métricas acumulativas',
  PRIMARY KEY (`id`),
  KEY `estado` (`estado`),
  KEY `variedad` (`variedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Informe_AnualidadAnterior`;
CREATE TABLE `Informe_AnualidadAnterior` (
  `informe` int(8) unsigned NOT NULL,
  `anualidad` year(4) NOT NULL,
  PRIMARY KEY (`informe`,`anualidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Informe_Metrica`;
CREATE TABLE `Informe_Metrica` (
  `informe` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`informe`,`metrica`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Informe_Prediccion`;
CREATE TABLE `Informe_Prediccion` (
  `informe` int(8) unsigned NOT NULL,
  `prediccion` int(8) unsigned NOT NULL,
  PRIMARY KEY (`informe`,`prediccion`),
  KEY `prediccion` (`prediccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Informe_Ubicacion`;
CREATE TABLE `Informe_Ubicacion` (
  `informe` int(8) unsigned NOT NULL,
  `ubicacion` int(8) unsigned NOT NULL,
  PRIMARY KEY (`informe`,`ubicacion`),
  KEY `ubicacion` (`ubicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Informe_Usuario`;
CREATE TABLE `Informe_Usuario` (
  `informe` int(8) unsigned NOT NULL,
  `usuario` int(8) unsigned NOT NULL,
  PRIMARY KEY (`informe`,`usuario`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `IntervaloAgrupacion`;
CREATE TABLE `IntervaloAgrupacion` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `acronimo` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acronimo` (`acronimo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Localizacion`;
CREATE TABLE `Localizacion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `parcela` int(8) unsigned NOT NULL,
  `tipo` enum('fenologia','incidencias') COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` decimal(10,7) NOT NULL,
  `longitud` decimal(10,7) NOT NULL,
  `altitud` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parcela_tipo_nombre` (`parcela`,`tipo`,`nombre`),
  KEY `latitud_longitud` (`latitud`,`longitud`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Localizacion_Estado`;
CREATE TABLE `Localizacion_Estado` (
  `localizacion` int(8) unsigned NOT NULL,
  `estado` int(8) unsigned NOT NULL,
  `anualidad` year(4) NOT NULL,
  `fecha` date NOT NULL,
  `imagen` int(8) unsigned DEFAULT NULL,
  `comentarios` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `_fechaModificacion` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`localizacion`,`estado`,`anualidad`),
  KEY `estado` (`estado`),
  KEY `imagen` (`imagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Mapa`;
CREATE TABLE `Mapa` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `acronimo` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `habilitado` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `acronimo` (`acronimo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Metrica`;
CREATE TABLE `Metrica` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `acronimo` varchar(8) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `unidad` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `grupo` int(4) unsigned NOT NULL,
  `orden` int(4) unsigned NOT NULL,
  `ordenApp` int(8) unsigned DEFAULT NULL,
  `prediccion` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `ocultar` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `ocultarNoDisponible` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `grafica` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`),
  UNIQUE KEY `grupo_orden` (`grupo`,`orden`),
  UNIQUE KEY `acronimo` (`acronimo`),
  UNIQUE KEY `ordenApp` (`ordenApp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `MetricaDerivada`;
CREATE TABLE `MetricaDerivada` (
  `metrica` int(8) unsigned NOT NULL,
  `parametros` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `diaria` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `acumulativa` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `calculable` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `atajoMetrica` int(8) unsigned DEFAULT NULL,
  `atajoFuncionAgrupacion` int(3) unsigned DEFAULT NULL,
  `atajoIntervaloAgrupacion` int(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`metrica`),
  UNIQUE KEY `atajo` (`atajoMetrica`,`atajoFuncionAgrupacion`,`atajoIntervaloAgrupacion`),
  KEY `atajoFuncionAgrupacion` (`atajoFuncionAgrupacion`),
  KEY `atajoIntervaloAgrupacion` (`atajoIntervaloAgrupacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `MetricaDerivada_Metrica`;
CREATE TABLE `MetricaDerivada_Metrica` (
  `metricaDerivada` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `requerida` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`metricaDerivada`,`metrica`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `MetricaGrupo`;
CREATE TABLE `MetricaGrupo` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Municipio`;
CREATE TABLE `Municipio` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `provincia` int(3) unsigned NOT NULL,
  `latitud` decimal(10,7) NOT NULL,
  `longitud` decimal(10,7) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_provincia` (`nombre`,`provincia`),
  KEY `FK_Municipio__Provincia` (`provincia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Nodo`;
CREATE TABLE `Nodo` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `numeroSerie` char(6) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `tipo` int(8) unsigned NOT NULL,
  `consolaRemota` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `notas` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `frecuenciaMuestreo` int(4) unsigned NOT NULL COMMENT 'En minutos',
  `frecuenciaMuestreoNueva` int(4) unsigned DEFAULT NULL COMMENT 'Nueva frecuencia deseada que se usará cuando el dispositivo sincronice (en minutos)',
  `actualizar` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT 'Indica si se debe actualizar la frecuencia de muestreo del nodo',
  `version` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaCompilacion` datetime DEFAULT NULL,
  `versionCompilador` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numeroSerie` (`numeroSerie`),
  KEY `tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `NodoAdcon`;
CREATE TABLE `NodoAdcon` (
  `nodo` int(8) unsigned NOT NULL,
  `adconId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `NodoAgZoom`;
CREATE TABLE `NodoAgZoom` (
  `nodo` int(8) unsigned NOT NULL,
  `agZoomId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `NodoBynse`;
CREATE TABLE `NodoBynse` (
  `nodo` int(10) unsigned NOT NULL,
  `imei` varchar(32) NOT NULL,
  PRIMARY KEY (`nodo`),
  UNIQUE KEY `imei` (`imei`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `NodoDecagon`;
CREATE TABLE `NodoDecagon` (
  `nodo` int(8) unsigned NOT NULL,
  `serial` char(8) NOT NULL,
  `ubicacion` int(8) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `lastRid` int(8) unsigned NOT NULL,
  `lastRecord` varchar(128) NOT NULL,
  PRIMARY KEY (`nodo`),
  UNIQUE KEY `ubicacion` (`ubicacion`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `serial` (`serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `NodoPessl`;
CREATE TABLE `NodoPessl` (
  `nodo` int(8) unsigned NOT NULL,
  `pesslId` char(8) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`nodo`),
  UNIQUE KEY `pesslId` (`pesslId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `NodoRanchSystems`;
CREATE TABLE `NodoRanchSystems` (
  `nodo` int(8) unsigned NOT NULL,
  `rsId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `NodoSico`;
CREATE TABLE `NodoSico` (
  `nodo` int(8) unsigned NOT NULL,
  `sicoId` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `NodoSigfox`;
CREATE TABLE `NodoSigfox` (
  `nodo` int(8) unsigned NOT NULL,
  `codigoSigfox` varchar(8) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `pac` char(16) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL COMMENT 'Porting Authorization Code',
  PRIMARY KEY (`nodo`),
  UNIQUE KEY `codigoSigfox` (`codigoSigfox`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Novedad`;
CREATE TABLE `Novedad` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `titulo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `textoCorto` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Markdown',
  `textoAmpliado` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Markdown',
  `ocultar` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ParametroCalibracion`;
CREATE TABLE `ParametroCalibracion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `sensor` int(8) unsigned NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `valorPorDefecto` decimal(12,5) unsigned DEFAULT NULL,
  `unidad` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sensor_nombre` (`sensor`,`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Parcela`;
CREATE TABLE `Parcela` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` int(8) unsigned NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `cultivo` int(8) unsigned NOT NULL,
  `variedad` int(8) unsigned DEFAULT NULL,
  `superficie` decimal(10,2) unsigned NOT NULL,
  `municipio` int(8) unsigned NOT NULL,
  `geoJson` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_nombre` (`usuario`,`nombre`),
  KEY `cultivo` (`cultivo`),
  KEY `municipio` (`municipio`),
  KEY `variedad` (`variedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ParcelaDatosVsim`;
CREATE TABLE `ParcelaDatosVsim` (
  `parcela` int(8) unsigned NOT NULL,
  `profundidadRaices` int(3) unsigned NOT NULL COMMENT 'cm',
  `potencialHidricoInicioEstres` int(3) unsigned NOT NULL,
  `potencialHidricoMaximoEstres` int(3) unsigned NOT NULL,
  `distanciaFilas` int(3) unsigned NOT NULL COMMENT 'cm',
  `distanciaPlantas` int(3) unsigned NOT NULL COMMENT 'cm',
  `humedadSueloInicial` decimal(5,2) unsigned NOT NULL COMMENT 'mm',
  `fechaInicioHumedadSuelo` date NOT NULL,
  PRIMARY KEY (`parcela`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ParcelaMapaDisponible`;
CREATE TABLE `ParcelaMapaDisponible` (
  `parcela` int(8) unsigned NOT NULL,
  `mapa` int(8) unsigned NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`parcela`,`mapa`,`fecha`),
  KEY `mapa` (`mapa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Prediccion`;
CREATE TABLE `Prediccion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `servicio` int(8) unsigned NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parametros` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `servicio_nombre` (`servicio`,`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Profesion`;
CREATE TABLE `Profesion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Provincia`;
CREATE TABLE `Provincia` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Red`;
CREATE TABLE `Red` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Regla`;
CREATE TABLE `Regla` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `alerta` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `operador` enum('>','<') COLLATE utf8_unicode_ci NOT NULL,
  `umbral` decimal(10,2) NOT NULL,
  `desde` int(3) unsigned DEFAULT NULL,
  `hasta` int(3) unsigned DEFAULT NULL,
  `minutos` int(4) unsigned DEFAULT NULL,
  `inicio` date DEFAULT NULL COMMENT 'Fecha de inicio, para métricas acumulativas',
  PRIMARY KEY (`id`),
  KEY `metrica` (`metrica`),
  KEY `alerta` (`alerta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Sensor`;
CREATE TABLE `Sensor` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `fabricante` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `referencia` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `notas` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `instruccionesMontaje` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Markdown',
  `grupo` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `grupo` (`grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `SensorCnode`;
CREATE TABLE `SensorCnode` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `idCnode` int(8) unsigned NOT NULL,
  `posicion` tinyint(1) unsigned DEFAULT NULL,
  `sensor` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idCnode_posicion` (`idCnode`,`posicion`),
  KEY `sensor` (`sensor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `SensorCnode_Metrica`;
CREATE TABLE `SensorCnode_Metrica` (
  `sensorCnode` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  `orden` int(8) unsigned NOT NULL,
  `minimo` decimal(8,3) NOT NULL,
  `maximo` decimal(8,3) NOT NULL,
  `precision` decimal(8,3) unsigned NOT NULL,
  `bits` int(2) unsigned NOT NULL,
  PRIMARY KEY (`sensorCnode`,`metrica`),
  UNIQUE KEY `sensorCnode_orden` (`sensorCnode`,`orden`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SensorGrupo`;
CREATE TABLE `SensorGrupo` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Sensor_Metrica`;
CREATE TABLE `Sensor_Metrica` (
  `sensor` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`sensor`,`metrica`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `ServicioPrediccion`;
CREATE TABLE `ServicioPrediccion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `acronimo` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `acronimo` (`acronimo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Software`;
CREATE TABLE `Software` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `placa` int(8) unsigned NOT NULL,
  `arduino` tinyint(1) unsigned NOT NULL,
  `version` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `commit` char(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `hex` mediumblob NOT NULL,
  `notas` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `placa_arduino_version` (`placa`,`arduino`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `TipoAcceso`;
CREATE TABLE `TipoAcceso` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `TipoArchivoUbicacion`;
CREATE TABLE `TipoArchivoUbicacion` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `TipoCultivo`;
CREATE TABLE `TipoCultivo` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `TipoNodo`;
CREATE TABLE `TipoNodo` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `TramaNodo`;
CREATE TABLE `TramaNodo` (
  `nodo` int(8) unsigned NOT NULL,
  `trama` int(8) unsigned NOT NULL,
  `fecha` datetime NOT NULL,
  `datos` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  PRIMARY KEY (`nodo`,`trama`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Ubicacion`;
CREATE TABLE `Ubicacion` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `latitud` decimal(10,7) NOT NULL,
  `longitud` decimal(10,7) NOT NULL,
  `altitud` int(4) unsigned NOT NULL,
  `primerDato` datetime DEFAULT NULL,
  `ultimoDato` datetime DEFAULT NULL,
  `banner` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ocultar` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `latitud_longitud` (`latitud`,`longitud`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UbicacionCliente`;
CREATE TABLE `UbicacionCliente` (
  `ubicacion` int(8) unsigned NOT NULL,
  `cliente` int(8) unsigned NOT NULL,
  `cultivo` int(8) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ubicacion`),
  KEY `cliente` (`cliente`),
  KEY `cultivo` (`cultivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UbicacionClienteComentario`;
CREATE TABLE `UbicacionClienteComentario` (
  `ubicacionCliente` int(8) unsigned NOT NULL,
  `fecha` date NOT NULL,
  `autor` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ubicacionCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UbicacionClienteVirtual`;
CREATE TABLE `UbicacionClienteVirtual` (
  `ubicacion` int(8) unsigned NOT NULL,
  `ubicacionClientePrincipal` int(8) unsigned NOT NULL,
  `orden` int(3) unsigned NOT NULL,
  PRIMARY KEY (`ubicacion`),
  UNIQUE KEY `ubicacionClientePrincipal_orden` (`ubicacionClientePrincipal`,`orden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UbicacionClienteVirtual_ConectorSensor`;
CREATE TABLE `UbicacionClienteVirtual_ConectorSensor` (
  `ubicacionClienteVirtual` int(8) unsigned NOT NULL,
  `conectorSensor` int(8) unsigned NOT NULL,
  PRIMARY KEY (`ubicacionClienteVirtual`,`conectorSensor`),
  UNIQUE KEY `conectorSensor` (`conectorSensor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UbicacionCliente_Archivo`;
CREATE TABLE `UbicacionCliente_Archivo` (
  `ubicacionCliente` int(8) unsigned NOT NULL,
  `archivo` int(8) unsigned NOT NULL,
  `tipo` int(8) unsigned NOT NULL,
  PRIMARY KEY (`ubicacionCliente`,`archivo`),
  UNIQUE KEY `archivo` (`archivo`),
  KEY `tipo` (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UbicacionCliente_Nodo`;
CREATE TABLE `UbicacionCliente_Nodo` (
  `ubicacionCliente` int(8) unsigned NOT NULL,
  `nodo` int(8) unsigned NOT NULL,
  `desde` datetime NOT NULL,
  `hasta` datetime DEFAULT NULL,
  PRIMARY KEY (`ubicacionCliente`,`desde`),
  UNIQUE KEY `nodo_desde` (`nodo`,`desde`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UbicacionCliente_Variedad`;
CREATE TABLE `UbicacionCliente_Variedad` (
  `ubicacion` int(8) unsigned NOT NULL,
  `variedad` int(8) unsigned NOT NULL,
  PRIMARY KEY (`ubicacion`,`variedad`),
  KEY `variedad` (`variedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UbicacionCliente_Variedad_Estado`;
CREATE TABLE `UbicacionCliente_Variedad_Estado` (
  `ubicacion` int(8) unsigned NOT NULL,
  `variedad` int(8) unsigned NOT NULL,
  `estado` int(8) unsigned NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`ubicacion`,`variedad`,`estado`,`fecha`),
  KEY `estado` (`estado`),
  KEY `variedad` (`variedad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UbicacionPublica`;
CREATE TABLE `UbicacionPublica` (
  `ubicacion` int(8) unsigned NOT NULL,
  `red` int(8) unsigned NOT NULL,
  PRIMARY KEY (`ubicacion`),
  KEY `red` (`red`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UbicacionPublica_Metrica`;
CREATE TABLE `UbicacionPublica_Metrica` (
  `ubicacionPublica` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`ubicacionPublica`,`metrica`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `UmbralEstresHidrico`;
CREATE TABLE `UmbralEstresHidrico` (
  `ubicacionCliente` int(8) unsigned NOT NULL,
  `profundidad` int(4) unsigned NOT NULL,
  `minimo` int(3) NOT NULL,
  `maximo` int(3) NOT NULL,
  PRIMARY KEY (`ubicacionCliente`,`profundidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UmbralRiego`;
CREATE TABLE `UmbralRiego` (
  `ubicacionCliente` int(8) unsigned NOT NULL,
  `minimo` int(3) unsigned NOT NULL,
  `maximo` int(3) unsigned NOT NULL,
  PRIMARY KEY (`ubicacionCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Usuario`;
CREATE TABLE `Usuario` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `cliente` int(8) unsigned DEFAULT NULL,
  `nombreUsuario` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `clave` char(60) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `nombre` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `administrador` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `tokenVerificacion` char(16) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `tokenRestablecerClave` char(16) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `terminosAceptados` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `tokenVerificacion` (`tokenVerificacion`),
  UNIQUE KEY `tokenRestablecerClave` (`tokenRestablecerClave`),
  UNIQUE KEY `nombreUsuario` (`nombreUsuario`),
  KEY `cliente` (`cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UsuarioAjustes`;
CREATE TABLE `UsuarioAjustes` (
  `usuario` int(8) unsigned NOT NULL,
  `compartirFenologia` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `compartirIncidencias` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UsuarioInformacion`;
CREATE TABLE `UsuarioInformacion` (
  `usuario` int(8) unsigned NOT NULL,
  `comoConociste` int(8) unsigned DEFAULT NULL,
  `comoConocisteTexto` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profesion` int(8) unsigned NOT NULL,
  `otraProfesion` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organizacion` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estudiante` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otroTipoCultivo` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sugerenciaRedesPublicas` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recibirBoletin` tinyint(1) unsigned NOT NULL,
  `informacionEstaciones` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`usuario`),
  KEY `profesion` (`profesion`),
  KEY `comoConociste` (`comoConociste`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UsuarioLicencia`;
CREATE TABLE `UsuarioLicencia` (
  `usuario` int(8) unsigned NOT NULL,
  `maximoUbicacionesPublicas` int(8) unsigned DEFAULT NULL,
  `maximoNumeroParcelas` int(8) unsigned DEFAULT NULL,
  `mapasHistoricosSinEstacion` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UsuarioOneSignal`;
CREATE TABLE `UsuarioOneSignal` (
  `usuario` int(8) unsigned NOT NULL,
  `oneSignalId` char(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `pushToken` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  PRIMARY KEY (`oneSignalId`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `UsuarioValorar`;
CREATE TABLE `UsuarioValorar` (
  `usuario` int(8) unsigned NOT NULL,
  `fechaValoracion` datetime DEFAULT NULL,
  `fechaSolicitud` datetime DEFAULT NULL,
  `ultimaSolicitud` datetime DEFAULT NULL,
  `numeroSolicitudes` int(10) unsigned DEFAULT 0,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Usuario_Metrica`;
CREATE TABLE `Usuario_Metrica` (
  `usuario` int(8) unsigned NOT NULL,
  `metrica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`usuario`,`metrica`),
  KEY `metrica` (`metrica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Usuario_TipoCultivo`;
CREATE TABLE `Usuario_TipoCultivo` (
  `usuario` int(8) unsigned NOT NULL,
  `tipoCultivo` int(8) unsigned NOT NULL,
  PRIMARY KEY (`usuario`,`tipoCultivo`),
  KEY `tipoCultivo` (`tipoCultivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Usuario_UbicacionCliente`;
CREATE TABLE `Usuario_UbicacionCliente` (
  `usuario` int(8) unsigned NOT NULL,
  `ubicacionCliente` int(8) unsigned NOT NULL,
  PRIMARY KEY (`usuario`,`ubicacionCliente`),
  KEY `ubicacionCliente` (`ubicacionCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Usuario_UbicacionPublica`;
CREATE TABLE `Usuario_UbicacionPublica` (
  `usuario` int(8) unsigned NOT NULL,
  `ubicacionPublica` int(8) unsigned NOT NULL,
  PRIMARY KEY (`usuario`,`ubicacionPublica`),
  KEY `ubicacionPublica` (`ubicacionPublica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `Variedad`;
CREATE TABLE `Variedad` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `cultivo` int(8) unsigned NOT NULL,
  `nombre` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cultivo_nombre` (`cultivo`,`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `Acceso`
  ADD CONSTRAINT `FK_Acceso__TipoAcceso` FOREIGN KEY (`tipo`) REFERENCES `TipoAcceso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Acceso__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Alerta`
  ADD CONSTRAINT `FK_Alerta__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `AlertaEnvio`
  ADD CONSTRAINT `FK_AlertaEnvio__Alerta` FOREIGN KEY (`alerta`) REFERENCES `Alerta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_AlertaEnvio__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `AlertaEnvioRegla`
  ADD CONSTRAINT `FK_AlertaEnvioRegla__AlertaEnvio` FOREIGN KEY (`alertaEnvio`) REFERENCES `AlertaEnvio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_AlertaEnvioRegla__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `AlertaHttpPost`
  ADD CONSTRAINT `FK_AlertaHttpPost__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Alerta_Ubicacion`
  ADD CONSTRAINT `FK_Alerta_Ubicacion__Alerta` FOREIGN KEY (`alerta`) REFERENCES `Alerta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Alerta_Ubicacion__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Archivo`
  ADD CONSTRAINT `FK_Archivo__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Autorizacion`
  ADD CONSTRAINT `FK_Autorizacion__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Autorizacion_UbicacionCliente`
  ADD CONSTRAINT `FK_Autorizacion_UbicacionCliente__Autorizacion` FOREIGN KEY (`autorizacion`) REFERENCES `Autorizacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Autorizacion_UbicacionCliente__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Autorizacion_Usuario`
  ADD CONSTRAINT `FK_Autorizacion_Usuario__Autorizacion` FOREIGN KEY (`autorizacion`) REFERENCES `Autorizacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Autorizacion_Usuario__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteAdcon`
  ADD CONSTRAINT `FK_ClienteAdcon__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteAdcon_Metrica`
  ADD CONSTRAINT `FK_ClienteAdcon_Metrica__ClienteAdcon` FOREIGN KEY (`clienteAdcon`) REFERENCES `ClienteAdcon` (`cliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ClienteAdcon_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteAgZoom`
  ADD CONSTRAINT `FK_ClienteAgZoom__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteBynse`
  ADD CONSTRAINT `FK_ClienteBynse__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteDecagon`
  ADD CONSTRAINT `FK_ClienteDecagon__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClientePessl`
  ADD CONSTRAINT `FK_ClientePessl__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteRanchSystems`
  ADD CONSTRAINT `FK_ClienteRanchSystems__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ClienteSico`
  ADD CONSTRAINT `FK_ClienteSico__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ComposicionSuelo`
  ADD CONSTRAINT `FK_ComposicionSuelo__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ComposicionSuelo__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ConectorSensor`
  ADD CONSTRAINT `FK_ConectorSensor__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ConectorSensor__Sensor` FOREIGN KEY (`sensor`) REFERENCES `Sensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ConectorSensor_ParametroCalibracion`
  ADD CONSTRAINT `FK_ConectorSensor_ParametroCalibracion__ConectorSensor` FOREIGN KEY (`conectorSensor`) REFERENCES `ConectorSensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ConectorSensor_ParametroCalibracion__ParametroCalibracion` FOREIGN KEY (`parametroCalibracion`) REFERENCES `ParametroCalibracion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Consulta`
  ADD CONSTRAINT `FK_Consulta__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ConsultaAnualidadAnterior`
  ADD CONSTRAINT `FK_ConsultaAnualidadAnterior__Consulta` FOREIGN KEY (`consulta`) REFERENCES `Consulta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Consulta_Metrica`
  ADD CONSTRAINT `FK_Consulta_Metrica__Consulta` FOREIGN KEY (`consulta`) REFERENCES `Consulta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Consulta_Metrica__FuncionAgrupacion` FOREIGN KEY (`funcionAgrupacion`) REFERENCES `FuncionAgrupacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Consulta_Metrica__IntervaloAgrupacion` FOREIGN KEY (`intervaloAgrupacion`) REFERENCES `IntervaloAgrupacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Consulta_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Consulta_Ubicacion`
  ADD CONSTRAINT `FK_Consulta_Ubicacion__Consulta` FOREIGN KEY (`consulta`) REFERENCES `Consulta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Consulta_Ubicacion__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Dato`
  ADD CONSTRAINT `FK_Dato__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Dato__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `DatoDerivado`
  ADD CONSTRAINT `FK_DatoDerivado__MetricaDerivada` FOREIGN KEY (`metrica`) REFERENCES `MetricaDerivada` (`metrica`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_DatoDerivado__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Email`
  ADD CONSTRAINT `FK_Email__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Enfermedad`
  ADD CONSTRAINT `FK_Enfermedad__Cultivo` FOREIGN KEY (`cultivo`) REFERENCES `Cultivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Estado`
  ADD CONSTRAINT `FK_Estado__Cultivo` FOREIGN KEY (`cultivo`) REFERENCES `Cultivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `GraficaConsultada`
  ADD CONSTRAINT `FK_GraficaConsultada__Acceso` FOREIGN KEY (`acceso`) REFERENCES `Acceso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `GraficaConsultada_AnualidadAnterior`
  ADD CONSTRAINT `FK_GraficaConsultada_AnualidadAnterior__GraficaConsultada` FOREIGN KEY (`graficaConsultada`) REFERENCES `GraficaConsultada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `GraficaConsultada_Metrica`
  ADD CONSTRAINT `FK_GraficaConsultada_Metrica__GraficaConsultada` FOREIGN KEY (`graficaConsultada`) REFERENCES `GraficaConsultada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_GraficaConsultada_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `GraficaConsultada_Ubicacion`
  ADD CONSTRAINT `FK_GraficaConsultada_Ubicacion__GraficaConsultada` FOREIGN KEY (`graficaConsultada`) REFERENCES `GraficaConsultada` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_GraficaConsultada_Ubicacion__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `IncidenciaEnfermedad`
  ADD CONSTRAINT `FK_IncidenciaEnfermedad__Archivo` FOREIGN KEY (`imagen`) REFERENCES `Archivo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IncidenciaEnfermedad__Enfermedad` FOREIGN KEY (`enfermedad`) REFERENCES `Enfermedad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IncidenciaEnfermedad__Localizacion` FOREIGN KEY (`localizacion`) REFERENCES `Localizacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `IncidenciaEnfermedadConteo`
  ADD CONSTRAINT `FK_IncidenciaEnfermedadConteo__Archivo` FOREIGN KEY (`imagen`) REFERENCES `Archivo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IncidenciaEnfermedadConteo__IncidenciaEnfermedad` FOREIGN KEY (`incidenciaEnfermedad`) REFERENCES `IncidenciaEnfermedad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Informe`
  ADD CONSTRAINT `FK_Informe__Estado` FOREIGN KEY (`estado`) REFERENCES `Estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Informe__Variedad` FOREIGN KEY (`variedad`) REFERENCES `Variedad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Informe_AnualidadAnterior`
  ADD CONSTRAINT `FK_Informe_AnualidadAnterior__Informe` FOREIGN KEY (`informe`) REFERENCES `Informe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Informe_Metrica`
  ADD CONSTRAINT `FK_Informe_Metrica__Informe` FOREIGN KEY (`informe`) REFERENCES `Informe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Informe_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Informe_Prediccion`
  ADD CONSTRAINT `FK_Informe_Prediccion__Informe` FOREIGN KEY (`informe`) REFERENCES `Informe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Informe_Prediccion__Prediccion` FOREIGN KEY (`prediccion`) REFERENCES `Prediccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Informe_Ubicacion`
  ADD CONSTRAINT `FK_Informe_Ubicacion__Informe` FOREIGN KEY (`informe`) REFERENCES `Informe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Informe_Ubicacion__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Informe_Usuario`
  ADD CONSTRAINT `FK_Informe_Usuario__Informe` FOREIGN KEY (`informe`) REFERENCES `Informe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Informe_Usuario__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Localizacion`
  ADD CONSTRAINT `FK_Localizacion__Parcela` FOREIGN KEY (`parcela`) REFERENCES `Parcela` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Localizacion_Estado`
  ADD CONSTRAINT `FK_Localizacion_Estado__Archivo` FOREIGN KEY (`imagen`) REFERENCES `Archivo` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Localizacion_Estado__Estado` FOREIGN KEY (`estado`) REFERENCES `Estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Localizacion_Estado__Localizacion` FOREIGN KEY (`localizacion`) REFERENCES `Localizacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Metrica`
  ADD CONSTRAINT `FK_Metrica__MetricaGrupo` FOREIGN KEY (`grupo`) REFERENCES `MetricaGrupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `MetricaDerivada`
  ADD CONSTRAINT `FK_MetricaDerivada__FuncionAgrupacion` FOREIGN KEY (`atajoFuncionAgrupacion`) REFERENCES `FuncionAgrupacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MetricaDerivada__IntervaloAgrupacion` FOREIGN KEY (`atajoIntervaloAgrupacion`) REFERENCES `IntervaloAgrupacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MetricaDerivada__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MetricaDerivada__Metrica_atajo` FOREIGN KEY (`atajoMetrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `MetricaDerivada_Metrica`
  ADD CONSTRAINT `FK_MetricaDerivada_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_MetricaDerivada_Metrica__MetricaDerivada` FOREIGN KEY (`metricaDerivada`) REFERENCES `MetricaDerivada` (`metrica`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Municipio`
  ADD CONSTRAINT `FK_Municipio__Provincia` FOREIGN KEY (`provincia`) REFERENCES `Provincia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Nodo`
  ADD CONSTRAINT `FK_Nodo__TipoNodo` FOREIGN KEY (`tipo`) REFERENCES `TipoNodo` (`id`) ON UPDATE CASCADE;

ALTER TABLE `NodoAdcon`
  ADD CONSTRAINT `FK_NodoAdcon__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoAgZoom`
  ADD CONSTRAINT `FK_NodoAgZoom__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoBynse`
  ADD CONSTRAINT `FK_NodoBynse__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoDecagon`
  ADD CONSTRAINT `FK_NodoDecagon__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_NodoDecagon__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoPessl`
  ADD CONSTRAINT `FK_NodoPessl__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoRanchSystems`
  ADD CONSTRAINT `FK_NodoRanchSystems__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoSico`
  ADD CONSTRAINT `FK_NodoSico__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `NodoSigfox`
  ADD CONSTRAINT `FK_NodoSigfox__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ParametroCalibracion`
  ADD CONSTRAINT `FK_ParametroCalibracion__Sensor` FOREIGN KEY (`sensor`) REFERENCES `Sensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Parcela`
  ADD CONSTRAINT `FK_Parcela__Cultivo` FOREIGN KEY (`cultivo`) REFERENCES `Cultivo` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Parcela__Municipio` FOREIGN KEY (`municipio`) REFERENCES `Municipio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Parcela__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Parcela__Variedad` FOREIGN KEY (`variedad`) REFERENCES `Variedad` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `ParcelaDatosVsim`
  ADD CONSTRAINT `FK_ParcelaDatosVsim__Parcela` FOREIGN KEY (`parcela`) REFERENCES `Parcela` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `ParcelaMapaDisponible`
  ADD CONSTRAINT `FK_ParcelaMapaDisponible__Mapa` FOREIGN KEY (`mapa`) REFERENCES `Mapa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ParcelaMapaDisponible__Parcela` FOREIGN KEY (`parcela`) REFERENCES `Parcela` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Prediccion`
  ADD CONSTRAINT `FK_Prediccion__ServicioPrediccion` FOREIGN KEY (`servicio`) REFERENCES `ServicioPrediccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Regla`
  ADD CONSTRAINT `FK_Regla__Alerta` FOREIGN KEY (`alerta`) REFERENCES `Alerta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Regla__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Sensor`
  ADD CONSTRAINT `FK_Sensor__SensorGrupo` FOREIGN KEY (`grupo`) REFERENCES `SensorGrupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `SensorCnode`
  ADD CONSTRAINT `FK_SensorCnode__Sensor` FOREIGN KEY (`sensor`) REFERENCES `Sensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `SensorCnode_Metrica`
  ADD CONSTRAINT `FK_SensorCnode_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_SensorCnode_Metrica__SensorCnode` FOREIGN KEY (`sensorCnode`) REFERENCES `SensorCnode` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Sensor_Metrica`
  ADD CONSTRAINT `FK_Sensor_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Sensor_Metrica__Sensor` FOREIGN KEY (`sensor`) REFERENCES `Sensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `TramaNodo`
  ADD CONSTRAINT `FK_TramaNodo__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionCliente`
  ADD CONSTRAINT `FK_UbicacionCliente__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente__Cultivo` FOREIGN KEY (`cultivo`) REFERENCES `Cultivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionClienteComentario`
  ADD CONSTRAINT `FK_UbicacionClienteComentario__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionClienteVirtual`
  ADD CONSTRAINT `FK_UbicacionClienteVirtual__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionClienteVirtual__UbicacionCliente` FOREIGN KEY (`ubicacionClientePrincipal`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionClienteVirtual_ConectorSensor`
  ADD CONSTRAINT `FK_UCV_CS__ConectorSensor` FOREIGN KEY (`conectorSensor`) REFERENCES `ConectorSensor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UCV_CS__UbicacionClienteVirtual` FOREIGN KEY (`ubicacionClienteVirtual`) REFERENCES `UbicacionClienteVirtual` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionCliente_Archivo`
  ADD CONSTRAINT `FK_UbicacionCliente_Archivo__Archivo` FOREIGN KEY (`archivo`) REFERENCES `Archivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente_Archivo__TipoArchivoUbicacion` FOREIGN KEY (`tipo`) REFERENCES `TipoArchivoUbicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente_Archivo__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionCliente_Nodo`
  ADD CONSTRAINT `FK_UbicacionCliente_Nodo__Nodo` FOREIGN KEY (`nodo`) REFERENCES `Nodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente_Nodo__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionCliente_Variedad`
  ADD CONSTRAINT `FK_UbicacionCliente_Variedad__UbicacionCliente` FOREIGN KEY (`ubicacion`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente_Variedad__Variedad` FOREIGN KEY (`variedad`) REFERENCES `Variedad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionCliente_Variedad_Estado`
  ADD CONSTRAINT `FK_UbicacionCliente_Variedad_Estado__Estado` FOREIGN KEY (`estado`) REFERENCES `Estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente_Variedad_Estado__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionCliente_Variedad_Estado__Variedad` FOREIGN KEY (`variedad`) REFERENCES `Variedad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionPublica`
  ADD CONSTRAINT `FK_UbicacionPublica__Red` FOREIGN KEY (`red`) REFERENCES `Red` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionPublica__Ubicacion` FOREIGN KEY (`ubicacion`) REFERENCES `Ubicacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UbicacionPublica_Metrica`
  ADD CONSTRAINT `FK_UbicacionPublica_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UbicacionPublica_Metrica__UbicacionPublica` FOREIGN KEY (`ubicacionPublica`) REFERENCES `UbicacionPublica` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UmbralEstresHidrico`
  ADD CONSTRAINT `FK_UmbralEstresHidrico__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UmbralRiego`
  ADD CONSTRAINT `FK_UmbralRiego__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Usuario`
  ADD CONSTRAINT `FK_Usuario__Cliente` FOREIGN KEY (`cliente`) REFERENCES `Cliente` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `UsuarioAjustes`
  ADD CONSTRAINT `FK_UsuarioAjustes__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UsuarioInformacion`
  ADD CONSTRAINT `FK_UsuarioInformacion__ComoConociste` FOREIGN KEY (`comoConociste`) REFERENCES `ComoConociste` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UsuarioInformacion__Profesion` FOREIGN KEY (`profesion`) REFERENCES `Profesion` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_UsuarioInformacion__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UsuarioLicencia`
  ADD CONSTRAINT `FK_UsuarioLicencia__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UsuarioOneSignal`
  ADD CONSTRAINT `FK_UsuarioOneSignal__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `UsuarioValorar`
  ADD CONSTRAINT `FK_UsuarioValorar__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Usuario_Metrica`
  ADD CONSTRAINT `FK_Usuario_Metrica__Metrica` FOREIGN KEY (`metrica`) REFERENCES `Metrica` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Usuario_Metrica__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Usuario_TipoCultivo`
  ADD CONSTRAINT `FK_Usuario_TipoCultivo__TipoCultivo` FOREIGN KEY (`tipoCultivo`) REFERENCES `TipoCultivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Usuario_TipoCultivo__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Usuario_UbicacionCliente`
  ADD CONSTRAINT `FK_Usuario_UbicacionCliente__UbicacionCliente` FOREIGN KEY (`ubicacionCliente`) REFERENCES `UbicacionCliente` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Usuario_UbicacionCliente__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Usuario_UbicacionPublica`
  ADD CONSTRAINT `FK_Usuario_UbicacionPublica__UbicacionPublica` FOREIGN KEY (`ubicacionPublica`) REFERENCES `UbicacionPublica` (`ubicacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Usuario_UbicacionPublica__Usuario` FOREIGN KEY (`usuario`) REFERENCES `Usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `Variedad`
  ADD CONSTRAINT `FK_Variedad__Cultivo` FOREIGN KEY (`cultivo`) REFERENCES `Cultivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
