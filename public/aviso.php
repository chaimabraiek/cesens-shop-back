<?php

include dirname(__DIR__) . '/bin/lib/mailer.php';

$msg = empty($_GET['msg']) ? '' : $_GET['msg'];
if (empty($msg)) {
    $msg = file_get_contents('php://input');
}
if (!empty($_GET['nodo'])) {
    $nodo = $_GET['nodo'];
    $nombreNodo = getNombreNodo($_GET['nodo']);
    if (!empty($nombreNodo)) {
        $nodo .= ' (' . $nombreNodo . ')';
    }
    $msg = $nodo . PHP_EOL . $msg;
}

$msg = implode(PHP_EOL . '----------------------------------' . PHP_EOL, explode(PHP_EOL, $msg));
$msg = implode(PHP_EOL, explode('_', $msg));

$mail = getMailer();
$mail->addReplyTo('no-responder@encore-lab.com');
$mail->addAddress('ivan@encore-lab.com');
$mail->addAddress('victor@encore-lab.com');
$mail->isHTML(false);
$mail->Subject = 'Cesens - Aviso' . (empty($_GET['nodo']) ? '' : ' (' . $_GET['nodo'] . ')');
$mail->Body = $msg;
$mail->send();

for ($i = 0; $i < 10; $i++) {
    echo 'DONE', PHP_EOL;
}
echo 'Date=', time() + (date('I') ? 7200 : 3600), PHP_EOL;

function getNombreNodo($nodo)
{
    $nodo = is_numeric($nodo) ? $nodo : substr($nodo, 4);
    static $pdo = null;
    if ($pdo === null) {
        $bd = include dirname(__DIR__) . '/config/bd.php';
        $pdo = new \PDO($bd['dsn'], $bd['user'], $bd['passwd'],
            [
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
                \PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
            ]);
    }
    $stm = $pdo->prepare('SELECT nombre FROM Ubicacion WHERE id = ?');
    if ($stm->execute([$nodo])) {
        $result = $stm->fetch();
        if (isset($result['nombre'])) {
            return $result['nombre'];
        }
    }
    return null;
}