<?php

namespace Encore\Formats;

use Encore\CesensApi\Model\Model;

class XMLSerializer
{
    public static function generateValidXmlFromObj(Model $obj, $node_block='nodes', $node_name='node')
    {
        $arr = get_object_vars($obj->jsonSerialize());
        return self::generateValidXmlFromArray($arr, $node_block, $node_name);
    }

    public static function generateValidXmlFromArray($array, $node_block='nodes', $node_name='node')
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<' . $node_block . '>';
        $xml .= self::generateXmlFromArray($array, $node_name);
        $xml .= '</' . $node_block . '>';
        return $xml;
    }

    private static function generateXmlFromArray($array, $node_name)
    {
        $xml = '';
        if ($array instanceof Model) {
            $array = $array->jsonSerialize();
        }
        if (is_array($array)) {
            foreach ($array as $key => $value) {
                if (is_numeric($key)) {
                    $key = $node_name;
                }
                $xml .= '<' . $key . '>' . self::generateXmlFromArray($value, $node_name) . '</' . $key . '>';
            }
        } else {
            $xml = htmlspecialchars($array, ENT_QUOTES);
        }
        return $xml;
    }
}