<?php

/** @var \Encore\CesensBlog\Model\Sigfox\TramaAbstractaServidor $object */
$object = isset($this->object) ? $this->object : null;

header('Content-Type: text/cesens-frame');
header('X-Unix-Timestamp: ' . time());

echo $object->toData();
