<?php

$data = isset($this->data) ? $this->data : null;
$contentType = isset($this->contentType) ? $this->contentType : null;
if ($data === null) {
    $archivo = isset($this->archivo) ? $this->archivo : null;
    $titulo = isset($this->titulo) ? $this->titulo : null;
    $borrar = isset($this->borrar) && $this->borrar;

    if (!file_exists($archivo) || !is_readable($archivo) || !is_file($archivo)) {
        throw new \Encore\Application\Controller\Exception\NotFoundException('Archivo no encontrado');
    }

    $contentType = $contentType ?: mime_content_type($archivo);
    $extension = pathinfo($archivo, PATHINFO_EXTENSION) ?: null;

    ignore_user_abort(true);
    header('Content-Type: ' . $contentType);
    header('Content-Disposition: ' . ($this->inline ? '' : 'attachment;') . 'filename="' . $titulo . ($extension !== null ? '.' . $extension : '') . '"');
    header('Cache-Control: max-age=0');
    readfile($archivo);

    if ($borrar) {
        unlink($archivo);
    }
} else {
    if (!empty($contentType)) {
        header('Content-Type: ' . $contentType);
    }
    echo $data;
}
