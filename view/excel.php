<?php

$titulo = isset($this->titulo) ? $this->titulo : null;
$format = isset($this->format) ? $this->format : null;
$excel  = isset($this->excel)  ? $this->excel  : null;

// formats
switch (strtolower($format)) {
    case 'xls':
    case 'excel2003':
        $extension = 'xls';
        $writerType = 'Excel5';
        $contentType = 'application/vnd.ms-excel';
        break;
    case 'csv':
        $extension = 'csv';
        $writerType = 'CSV';
        $contentType = 'text/csv';
        break;
    case 'html':
    case 'htm':
        $extension = 'html';
        $writerType = 'HTML';
        $contentType = 'text/html';
        break;
    default:
        $extension = 'xlsx';
        $writerType = 'Excel2007';
        $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
}

// send headers to force navigator to download the generated file
header('Content-Type: ' . $contentType);
header('Content-Disposition: attachment;filename="' . $titulo . '.' . $extension . '"');
header('Cache-Control: max-age=0');

// put the file into output
$objWriter = \PHPExcel_IOFactory::createWriter($excel, $writerType);
$objWriter->save('php://output');