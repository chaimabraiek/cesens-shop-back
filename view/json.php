<?php

$object = isset($this->object) ? $this->object : null;

header('Content-Type: application/json;charset=utf-8');
echo json_encode($object, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
