<?php

$object = isset($this->object) ? $this->object : null;

header('Content-Type: text/plain;charset=utf-8');
header('X-Unix-Timestamp: ' . (time() + (date('I') ? 7200 : 3600)));

echo 'DONE', "\r\n";
if ($object !== null) {
    if ($object instanceof \Encore\CesensBlog\Model\Model) {
        $object = $object->jsonSerialize();
    } elseif (is_object($object)) {
        $object = (array)$object;
    } elseif (!is_array($object)) {
        $object = [
            'msg' => $object,
        ];
    }
    foreach ($object as $key => $value) {
        if (!is_object($value) && !is_array($value)) {
            echo $key, "\t", str_replace('\'', '', var_export($value, true)), "\r\n";
        }
    }
}
