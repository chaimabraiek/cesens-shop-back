<?php

use Encore\Formats\XMLSerializer;

$object = isset($this->object) ? $this->object : null;

if (is_array($object)) {
    $string = XMLSerializer::generateValidXmlFromArray($object);
} elseif (is_object($object)) {
    $string = XMLSerializer::generateValidXmlFromObj($object);
} else {
    die('Not supported');
}

header('Content-Type: text/xml;charset=utf-8');
echo $string;
